package zen.view;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import zen.controller.CellListener;
import zen.model.PathConfig;
import zen.model.PawnColor;
import zen.view.components.WindowImage;

public class CellPanel extends JPanel {

	private static final long serialVersionUID = -126431137146671790L;
	private final int IMAGE_SIZE = 63;
	
	private WindowImage mainImage;
	private PawnColor color;
	private Color borderColor;
	
	public CellPanel(PawnColor color, boolean background) {
		this.setColor(color);
		
		this.initPanel();
		this.updateComponents(background);
	}
	
	public PawnColor getColor() {
		return this.color;
	}
	
	public void setColor(PawnColor color) {
		this.color = color;
	}
	
	public Color getBorderColor() {
		return borderColor;
	}

	public void setNewBorder(Color color) {
		this.borderColor = color;
		this.setBorder(BorderFactory.createLineBorder(color, 2));
	}
	
	private void initPanel() {
		this.setLayout(new GridLayout(1, 1));
		this.mainImage = new WindowImage(this, PathConfig.WOOD_PATH + "1.png", this.IMAGE_SIZE);
		this.add(this.mainImage);
	}
	
	/**
	 * Update the panel
	 * @param background True background 1, false background 2
	 */
	public void updateComponents(boolean background) {
		int woodColor = 2;
		if (background) woodColor = 1;
		
		if (this.color == PawnColor.BLACK)
			this.mainImage.changeIcon(PathConfig.WOOD_PATH + woodColor + "_black.png");
		else if (this.color == PawnColor.WHITE)
			this.mainImage.changeIcon(PathConfig.WOOD_PATH + woodColor + "_white.png");
		else if (this.color == PawnColor.ZEN)
			this.mainImage.changeIcon(PathConfig.WOOD_PATH + woodColor + "_zen.png");
		else if (this.color == PawnColor.VOID)
			this.mainImage.changeIcon(PathConfig.WOOD_PATH + woodColor + ".png");

		this.setNewBorder(CellListener.DEFAULTCOLOR);
	}
}
