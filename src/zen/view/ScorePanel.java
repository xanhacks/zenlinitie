package zen.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import zen.controller.ScorePanelListener;
import zen.model.Game;
import zen.model.PathConfig;
import zen.model.PawnColor;
import zen.view.components.GameInfoPanel;
import zen.view.components.WindowImage;
import zen.view.components.WindowText;

public class ScorePanel extends JPanel {

	private static final long serialVersionUID = 4745883682496704409L;
	public static final int iconsSize = 64;

	private GameInfoPanel currentPlayerPanel;
	private JPanel iconsPanel;
	
	private WindowText statusText;
	private WindowText currentPlayerText;
	private WindowText whitePawnsLeftText;
	private WindowText blackPawnsLeftText;
	
	private WindowImage homeIcon;
	private WindowImage soundIcon;
	private WindowImage restartIcon;

	public ScorePanel() {
		this.displayComponents();
	}
	
	/**
	 * Add the background to the JPanel
	 */
    public void paintComponent(Graphics g) {
		try {
			BufferedImage image = ImageIO.read(getClass().getClassLoader().getResource(PathConfig.BACKGROUND_PATH));
			g.drawImage(image, 0, 0, null);
		} catch (IOException e) {}
    }
    
    public WindowImage getHomeIcon() {
    	return this.homeIcon;
    }
    
    public WindowImage getSoundIcon() {
    	return this.soundIcon;
    }
    
    public WindowImage getRestartIcon() {
    	return this.restartIcon;
    }

	/**
	 * Change text of the current player label
	 * @param currentPlayer The new text
	 */
	private void setTextCurrentPlayer(String currentPlayerName, PawnColor currentPlayerColor) {
		if (this.currentPlayerText != null) {
			this.currentPlayerText.setText(currentPlayerName.toString());
			
			if (currentPlayerColor == PawnColor.WHITE)
				this.currentPlayerText.setForeground(Color.white);
			else
				this.currentPlayerText.setForeground(Color.black);
		}
			
	}

	/**
	 * Change the number of white pawns left.
	 * @param whitePawnsLeft The new number
	 */
	private void setTextWhitePawnsLeft(int whitePawnsLeft) {
		if (this.whitePawnsLeftText != null)
			this.whitePawnsLeftText.setText("White pawns: " + whitePawnsLeft);
	}

	/**
	 * Change the number of black pawns left.
	 * @param blackPawnsLeft The new number
	 */
	private void setTextBlackPawnsLeft(int blackPawnsLeft) {
		if (this.blackPawnsLeftText != null)
			this.blackPawnsLeftText.setText("Black pawns: " + blackPawnsLeft);
	}
	
	/**
	 * Set the new icon for the sound
	 * @param activate True if the sound is ON else false
	 */
	public void setSoundIcon(boolean activate) {
		if (activate) this.soundIcon.changeIcon(PathConfig.SOUND_ICON_PATH);
		else this.soundIcon.changeIcon(PathConfig.NOSOUND_ICON_PATH);
	}
	
	/**
	 * Display the winner on the window
	 * @param winner The name of winner
	 */
	public void setWinner(String winner) {
		this.currentPlayerText.setText(winner);

		if (winner.equalsIgnoreCase("WHITE")) this.currentPlayerText.setForeground(Color.WHITE);
		else if (winner.equalsIgnoreCase("BLACK")) this.currentPlayerText.setForeground(Color.BLACK);
		else this.currentPlayerText.setForeground(Color.YELLOW);
		
		this.statusText.setText("Winner:");
		this.statusText.setNewFont(40, true);
		this.statusText.setForeground(Color.yellow);
		
		this.iconsPanel.add(this.restartIcon, 0);
		this.iconsPanel.remove(this.homeIcon);
		this.iconsPanel.updateUI();
	}
	
	/**
	 * Reset the winner text
	 */
	public void resetWinner() {
		this.statusText.setText("Current player:");
		this.statusText.setNewFont(38, false);
		this.statusText.setForeground(Color.black);
		
		this.iconsPanel.add(this.homeIcon, 0);
		this.iconsPanel.remove(this.restartIcon);
		this.iconsPanel.updateUI();
	}
	
	/**
	 * Update components of the window.
	 */
	public void updateComponents(String currentPlayerName, PawnColor currentPlayerColor, int nbWhiteLeft, int nbBlackLeft) {
		if (currentPlayerName != null) {
			this.setTextCurrentPlayer(currentPlayerName, currentPlayerColor);
			this.setTextWhitePawnsLeft(nbWhiteLeft);
			this.setTextBlackPawnsLeft(nbBlackLeft);
		}
	}

	/**
	 * Display the components on the JPanel.
	 */
	private void displayComponents() {
		this.setBorder(new EmptyBorder(0, 30, 0, 30));
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		int margin = 35;

		// Game icon
		WindowImage gameIcon = new WindowImage(this, PathConfig.GAME_ICON_PATH, 220);
		gameIcon.setNewMargin((int)1.8*margin, 0, margin, 0);
		
		// Current player
		this.currentPlayerPanel = new GameInfoPanel(2, 1, 435, 145);
		this.currentPlayerPanel.setNewBorder(4);
		
		this.statusText = new WindowText("Current player:");
		this.statusText.setNewFont(38, false);
		this.statusText.setForeground(Color.black);

		this.currentPlayerText = new WindowText("");
		this.currentPlayerText.setNewFont(40, true);
		
		this.currentPlayerPanel.add(this.statusText);
		this.currentPlayerPanel.add(this.currentPlayerText);

		// Pawns left number
		GameInfoPanel pawnsNumberPanel = new GameInfoPanel(2, 1, 435, 135);
		pawnsNumberPanel.setNewBorder(3);
		
		this.blackPawnsLeftText = new WindowText("Black pawns: ");
		this.blackPawnsLeftText.setNewFont(38, false);
		this.blackPawnsLeftText.setForeground(Color.black);

		this.whitePawnsLeftText = new WindowText("White pawns: ");
		this.whitePawnsLeftText.setNewFont(38, false);
		this.whitePawnsLeftText.setForeground(Color.white);

		pawnsNumberPanel.add(this.blackPawnsLeftText);
		pawnsNumberPanel.add(this.whitePawnsLeftText);
		
		// Home & sound icons
		this.iconsPanel = new GameInfoPanel(1, 3, iconsSize*3, iconsSize);

		this.homeIcon = new WindowImage(this, PathConfig.HOME_ICON_PATH, iconsSize);
		this.soundIcon = new WindowImage(this, PathConfig.SOUND_ICON_PATH, iconsSize);
		this.restartIcon = new WindowImage(this, PathConfig.RESTART_ICON_PATH, iconsSize);

		this.iconsPanel.add(this.homeIcon);
		this.iconsPanel.add(new GameInfoPanel(1, 1, iconsSize, iconsSize));
		this.iconsPanel.add(this.soundIcon);

		this.add(gameIcon);
		this.add(Box.createRigidArea(new Dimension(0, margin)));
		this.add(this.currentPlayerPanel);
		this.add(Box.createRigidArea(new Dimension(0, (int)1.2*margin)));
		this.add(pawnsNumberPanel);
		this.add(Box.createRigidArea(new Dimension(0, (int)2*margin)));
		this.add(this.iconsPanel);
	}
	
	public void addListener(GameWindow gameWindow, Game game) {
		ScorePanelListener scorePanelListener = new ScorePanelListener(this, gameWindow, game);
		
		this.homeIcon.addMouseListener(scorePanelListener);
		this.soundIcon.addMouseListener(scorePanelListener);
		this.restartIcon.addMouseListener(scorePanelListener);
	}


}