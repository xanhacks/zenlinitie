package zen.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import zen.model.Game;
import zen.model.PathConfig;
import zen.model.PawnColor;

public class GameWindow extends JFrame {

	private static final long serialVersionUID = 4585650133502609789L;

	private BoardPanel boardPanel;
	private ScorePanel scorePanel;
	private Menu menu;

	/**
	 * Create the Game Window.
	 * @param boardPanel The left part of the window.
	 * @param scorePanel The right part of the window.
	 */
	public GameWindow(Game game, BoardPanel boardPanel, ScorePanel scorePanel) {
		if (game != null && boardPanel != null && scorePanel != null) {

			this.boardPanel = boardPanel;
			this.scorePanel = scorePanel;
			this.menu = null;

			this.initFrame();
			this.addMenu(game);
			this.addListeners(game);
			this.displayComponents();

			this.setVisible(true);
			
		} else {
			JOptionPane.showMessageDialog(this, "Could not load the game window !", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public Menu getMenu() {
		return this.menu;
	}

	private void initFrame() {
		this.setTitle("Zen L'initie : In game");
		this.setSize(1408, 920);
		this.setMinimumSize(new Dimension(1408, 920));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}
	
	private void addMenu(Game game) {
		this.menu = new Menu();
		this.menu.addListeners(this, game);
		
		this.setJMenuBar(this.menu);
	}
	
	private void addListeners(Game game) {
		this.boardPanel.addListeners(game, this.scorePanel);
		this.scorePanel.addListener(this, game);
	}

	/**
	 * Display the components on the window.
	 */
	private void displayComponents() {
		try {
			URL url = getClass().getClassLoader().getResource(PathConfig.BACKGROUND_PATH);
			this.setContentPane(new JLabel(new ImageIcon(ImageIO.read(url))));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.setLayout(new BorderLayout());
		this.add(this.boardPanel, BorderLayout.WEST);
		this.add(this.scorePanel, BorderLayout.CENTER);
	}
	
	/**
	 * Refresh the components of the window.
	 */
	public void refreshWindow(String currentPlayerName, PawnColor currentPlayerColor, int nbWhiteLeft, int nbBlackLeft) {
		this.boardPanel.getCellListener().refreshAllPanels();
		this.scorePanel.resetWinner();
		this.scorePanel.updateComponents(currentPlayerName, currentPlayerColor, nbWhiteLeft, nbBlackLeft);
	}

}