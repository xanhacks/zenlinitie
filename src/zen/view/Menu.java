package zen.view;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import zen.controller.MyMenuListener;
import zen.model.Game;

/**
 * The menu displayed on each window (except Rules and About).
 * 
 * @author Boris LICKINDORF - C2
 */
public class Menu extends JMenuBar {

	private static final long serialVersionUID = 3659374840117149978L;
	
	private MyMenuListener myMenuListener;
	
	private JMenuItem newMenuItem;
	private JMenuItem openMenuItem;
	private JMenuItem saveMenuItem;
	private JMenuItem exitMenuItem;
	private JMenuItem rulesMenuItem;
	private JMenuItem aboutMenuItem;

	public Menu() {
		this.displayMenuBar();
	}

	public JMenuItem getNewMenuItem() {
		return this.newMenuItem;
	}

	public JMenuItem getOpenMenuItem() {
		return this.openMenuItem;
	}

	public JMenuItem getSaveMenuItem() {
		return this.saveMenuItem;
	}

	public JMenuItem getExitMenuItem() {
		return this.exitMenuItem;
	}

	public JMenuItem getRulesMenuItem() {
		return this.rulesMenuItem;
	}

	public JMenuItem getAboutMenuItem() {
		return this.aboutMenuItem;
	}
	
	public MyMenuListener getListener() {
		return this.myMenuListener;
	}

	/**
	 * Create the menu by adding all the menuItem.
	 */
	private void displayMenuBar() {
		JMenu fileMenu = new JMenu("Game");
		
	    this.newMenuItem = new JMenuItem("Restart game");
	    newMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
	    fileMenu.add(newMenuItem);
	    
	    this.openMenuItem = new JMenuItem("Load a Game");
	    openMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
	    fileMenu.add(openMenuItem);

	    this.saveMenuItem = new JMenuItem("Save");
	    saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
	    fileMenu.add(saveMenuItem);

	    this.exitMenuItem = new JMenuItem("Quit");
	    exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));
	    fileMenu.add(exitMenuItem);
	    
		JMenu aboutMenu = new JMenu("About"); 
	    
	    this.rulesMenuItem = new JMenuItem("Rules");
	    rulesMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
	    aboutMenu.add(rulesMenuItem);
	    
	    this.aboutMenuItem = new JMenuItem("About");
	    aboutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
	    aboutMenu.add(aboutMenuItem);

	    this.add(fileMenu);
	    this.add(aboutMenu);
	}

	/**
	 * Add listeners on the menu.
	 * @param window The current window of the menu.
	 * @param game The game object.
	 */
	public void addListeners(JFrame window, Game game) {
		this.myMenuListener = new MyMenuListener(window, this, game);
		
		this.newMenuItem.addActionListener(this.myMenuListener);
		this.openMenuItem.addActionListener(this.myMenuListener);
		this.saveMenuItem.addActionListener(this.myMenuListener);
		this.exitMenuItem.addActionListener(this.myMenuListener);
		this.rulesMenuItem.addActionListener(this.myMenuListener);
		this.aboutMenuItem.addActionListener(this.myMenuListener);
	}

}