package zen.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import zen.model.PathConfig;
import zen.view.components.WindowImage;
import zen.view.components.WindowText;

public class RulesWindow extends JFrame {

	private static final long serialVersionUID = 7532922866090619876L;

	/**
	 * Initialize the JFrame and call displayComponents.
	 */
	public RulesWindow(String rules) {
		if (rules != null) {
			this.initFrame();
			this.displayComponents(rules);
			
			this.setVisible(true);
		} else {
			JOptionPane.showMessageDialog(this, "Could not read the rules !", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void initFrame() {
		this.setTitle("Rules");
		this.setSize(1000, 670);
		this.setMinimumSize(new Dimension(1000, 670));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(new BorderLayout());
		this.setLocationRelativeTo(null);
	}

	/**
	 * Display all the components on the window.
	 */
	private void displayComponents(String rules) {
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new FlowLayout());

		WindowImage gameIcon = new WindowImage(this, PathConfig.GAME_ICON_PATH, 80);
		gameIcon.setNewMargin(20, 20, 20, 20);
		
		WindowText titleText = new WindowText("Rules of the game");
		titleText.setNewFont(20, true);

		JTextArea rulesText = new JTextArea(rules);
		rulesText.setBorder(new EmptyBorder(10, 15, 0, 15));
		rulesText.setFont(new Font("MONOSPACED", Font.PLAIN, 15));
		rulesText.setBackground(new Color(240, 240, 240));
		rulesText.setLineWrap(true);
		rulesText.setEditable(false);

		topPanel.add(gameIcon);
		topPanel.add(titleText);
		this.add(topPanel, BorderLayout.NORTH);
		this.add(rulesText, BorderLayout.CENTER);;
	}

}