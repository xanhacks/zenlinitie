package zen.view;

import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import zen.controller.FirstWindowListener;
import zen.model.Game;
import zen.model.PathConfig;
import zen.view.components.WindowButton;
import zen.view.components.WindowImage;

public class FirstWindow extends JFrame {

	private static final long serialVersionUID = -893697232179941321L;

	private Menu menu;

	private WindowButton playWithIA;
	private WindowButton playWithPlayer;
	private WindowButton showRulesBtn;
	private WindowButton exitBtn;

	public FirstWindow() {
		this.initFrame();
		this.displayComponents();
	}

	public WindowButton getPlayWithIA() {
		return playWithIA;
	}

	public WindowButton getPlayWithPlayer() {
		return playWithPlayer;
	}

	public WindowButton getShowRulesBtn() {
		return showRulesBtn;
	}

	public WindowButton getExitBtn() {
		return exitBtn;
	}
	
	private void initFrame() {
		this.setTitle("Zen l'initie : Choose a game mode");
		this.setSize(768, 830);
		this.setMinimumSize(new Dimension(768, 830));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}

	/**
	 * Display components on the window
	 */
	private void displayComponents() {
		try {
			URL url = getClass().getClassLoader().getResource(PathConfig.BACKGROUND_PATH);
			this.setContentPane(new JLabel(new ImageIcon(ImageIO.read(url))));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.setMinimumSize(new Dimension(768, 830));
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		
		int marginSize = 22;
		int iconSize = 220;

		WindowImage iconImage = new WindowImage(this, PathConfig.GAME_ICON_PATH, iconSize);
		iconImage.setNewMargin(marginSize*2, 0, marginSize*2, 0);
		
		this.playWithIA = new WindowButton("Play VS IA");
		this.playWithIA.setNewFont(55, true);
		this.playWithIA.setNewBorder(12, 30, 12, 30, Color.black, 5);
		
		this.playWithPlayer = new WindowButton("Play VS Player");
		this.playWithPlayer.setNewFont(52, false);
		this.playWithPlayer.setNewBorder(12, 25, 12, 25, Color.black, 5);
		
		this.showRulesBtn = new WindowButton("Rules");
		this.showRulesBtn.setNewFont(50, false);
		this.showRulesBtn.setNewBorder(12, 25, 12, 25, Color.black, 5);
		
		this.exitBtn = new WindowButton("Exit");
		this.exitBtn.setNewFont(50, false);
		this.exitBtn.setNewBorder(12, 25, 12, 25, Color.black, 5);

		this.add(iconImage);
		this.add(this.playWithIA);
		this.add(Box.createRigidArea(new Dimension(0, marginSize)));
		this.add(this.playWithPlayer);
		this.add(Box.createRigidArea(new Dimension(0, marginSize)));
		this.add(this.showRulesBtn);
		this.add(Box.createRigidArea(new Dimension(0, marginSize)));
		this.add(this.exitBtn);

	}

	public void addMenu(Game game) {
		this.menu = new Menu();
		this.menu.addListeners(this, game);
		
		this.setJMenuBar(this.menu);
	}

	/**
	 * Add listeners on the buttons
	 */
	public void addListeners(Game game) {
		FirstWindowListener firstWindowListener = new FirstWindowListener(this, game);
		
		this.playWithIA.addActionListener(firstWindowListener);
		this.playWithPlayer.addActionListener(firstWindowListener);
		this.showRulesBtn.addActionListener(firstWindowListener);
		this.exitBtn.addActionListener(firstWindowListener);
	}

}