package zen.view;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

import zen.model.PathConfig;
import zen.view.components.WindowImage;
import zen.view.components.WindowText;

public class AboutWindow extends JFrame {

	private static final long serialVersionUID = -6520916646115007705L;

	public AboutWindow() {
		this.initFrame();
		this.displayComponents();
		
		this.setVisible(true);
	}
	
	private void initFrame() {
		this.setTitle("About");
		
		this.setSize(300, 350);
		this.setMinimumSize(new Dimension(300, 350));
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(new FlowLayout(FlowLayout.CENTER));
		this.setLocationRelativeTo(null);
	}

	private void displayComponents() {				
		WindowImage gameIcon = new WindowImage(this, PathConfig.GAME_ICON_PATH, 180);
		gameIcon.setNewMargin(20, 30, 20, 30);
		
		WindowText titleText = new WindowText("Zen L'initie");
		titleText.setNewFont(20, true);
		titleText.setBorder(new EmptyBorder(0, 0, 10, 0));
		
		WindowText authorText = new WindowText("Author: @xanhacks (gitlab)");
		authorText.setNewFont(15, false);
		
		this.add(gameIcon);
		this.add(titleText);
		this.add(authorText);
	}

}