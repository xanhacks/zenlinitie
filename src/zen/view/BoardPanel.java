package zen.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import zen.controller.CellListener;
import zen.model.Cell;
import zen.model.Game;
import zen.model.PathConfig;
import zen.view.components.GameInfoPanel;
import zen.view.components.WindowText;

public class BoardPanel extends JPanel {

	private static final long serialVersionUID = 4728675299974226736L;
	
	private CellListener cellListener;
	private CellPanel[][] cellPanel;
	
	public BoardPanel(Cell[][] board, ScorePanel sc) {
		if (board != null && sc != null) {
			this.cellPanel = new CellPanel[Game.SIZE][Game.SIZE];
			this.displayComponents(board);
		} else {
			JOptionPane.showMessageDialog(this, "Could not load the board panel !", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public CellListener getCellListener() {
		return this.cellListener;
	}
	
	/**
	 * Add the background to the JPanel
	 */
    public void paintComponent(Graphics g) {
		try {
			URL url = getClass().getClassLoader().getResource(PathConfig.BACKGROUND_PATH);
			BufferedImage image = ImageIO.read(url);
			g.drawImage(image, 0, 0, null);
		} catch (IOException e) {}
    }

	/**
	 * Create and display the board.
	 */
	private void displayComponents(Cell[][] board) {		
		JPanel boardPanel = new JPanel();
		boardPanel.setLayout(new GridLayout(Game.SIZE+2, Game.SIZE+2));
		
		for (int i = 0; i < Game.SIZE+2; i++) {
			for (int j = 0; j < Game.SIZE+2; j++) {
				if ((i == 0 && j == 0) || (i == Game.SIZE+1 && j == Game.SIZE+1) || (i == Game.SIZE+1 && j == 0) || (i == 0 && j == Game.SIZE+1)) {
					boardPanel.add(new GameInfoPanel(1, 1, 0, 0));
				} else if (i == 0) {
					boardPanel.add(this.boardLabel(j, true));
				} else if (j == 0) {
					boardPanel.add(this.boardLabel(i, false));
				} else if (i == Game.SIZE+1) {
					boardPanel.add(this.boardLabel(j, true));
				} else if (j == Game.SIZE+1) {
					boardPanel.add(this.boardLabel(i, false));
				} else {
					if ((i+j-2) % 2 == 0) this.cellPanel[i-1][j-1] = new CellPanel(board[i-1][j-1].getColor(), true);
					else this.cellPanel[i-1][j-1] = new CellPanel(board[i-1][j-1].getColor(), false);
					boardPanel.add(this.cellPanel[i-1][j-1]);
				}
				
			}
		}
		
		boardPanel.setOpaque(false);
		this.add(boardPanel);
	}
	
	/**
	 * Create the JLabel for the board
	 * @param number The number of the case in the board
	 * @param letter If yes, it's a letter else it's a number
	 * @return The JLabel to display on the window
	 */
	private WindowText boardLabel(int number, boolean letter) {
		WindowText ret = null;
		
		HashMap<Integer, String> intToChar = new HashMap<Integer, String>();
		intToChar.put(0, "A");
		intToChar.put(1, "B");
		intToChar.put(2, "C");
		intToChar.put(3, "D");
		intToChar.put(4, "E");
		intToChar.put(5, "F");
		intToChar.put(6, "G");
		intToChar.put(7, "H");
		intToChar.put(8, "I");
		intToChar.put(9, "J");
		intToChar.put(10, "K");
		
		if (letter) ret = new WindowText(intToChar.get(number-1));
		else ret = new WindowText(Integer.toString(11+1-number));

		ret.setNewFont(35, true);
		ret.setForeground(Color.black);
		
		return ret;
	}

	/**
	 * Add a listener for all the cells
	 * @param sc The ScorePanel object of the GameWindow
	 */
	public void addListeners(Game game, ScorePanel sc) {
		this.cellListener = new CellListener(game, this.cellPanel, sc);
		
		for (int i = 0; i < Game.SIZE; i++) {
			for (int j = 0; j < Game.SIZE; j++) {
				this.cellPanel[i][j].addMouseListener(this.cellListener);
			}
		}
	}

}