package zen.view.components;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

public class WindowImage extends JLabel {

	private static final long serialVersionUID = 5223181700741588170L;
	private Object obj;
	private int imageSize;

	public WindowImage(Object obj, String imagePath, int imageSize) {
		super(new ImageIcon(new ImageIcon(obj.getClass().getClassLoader().getResource(imagePath)).getImage().getScaledInstance(imageSize, imageSize, Image.SCALE_DEFAULT)));

		this.obj = obj;
		this.imageSize = imageSize;
		
		this.initializeDisplay();
	}
	
	private void initializeDisplay() {
		this.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		this.setAlignmentY(JLabel.CENTER_ALIGNMENT);
	}
	
	public void setNewMargin(int top, int left, int bottom, int right) {
		this.setBorder(new EmptyBorder(top, left, bottom, right));
	}
	
	public void changeIcon(String imagePath) {
		this.setIcon(new ImageIcon(new ImageIcon(this.obj.getClass().getClassLoader().getResource(imagePath)).getImage().getScaledInstance(this.imageSize, this.imageSize, Image.SCALE_DEFAULT)));
	}
}
