package zen.view.components;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class WindowText extends JLabel {
	
	private static final long serialVersionUID = -6331049850676811163L;

	public WindowText(String text) {
		super(text, SwingConstants.CENTER);
	}
	
	public void setNewFont(int size, boolean bold) {
		if (size > 0) {
			if (bold) this.setFont(new Font("MONOSPACED", Font.BOLD, size));
			else this.setFont(new Font("MONOSPACED", Font.PLAIN, size));
		}
	}

}
