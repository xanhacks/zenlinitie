package zen.view.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class GameInfoPanel extends JPanel {

	private static final long serialVersionUID = -4429748223962824022L;

	public GameInfoPanel(int rows, int cols, int width, int height) {
		this.initPanel(rows, cols);
		this.setMaxDimension(width, height);
	}

	private void initPanel(int rows, int cols) {
		this.setLayout(new GridLayout(rows, cols));
		this.setOpaque(false);
	}
	
	private void setMaxDimension(int width, int height) {
		if (width >= 0 && height >= 0) {
			this.setMaximumSize(new Dimension(width, height));
		}
	}
	
	public void setNewBorder(int thickness) {
		this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK, thickness), 
				BorderFactory.createEmptyBorder(12, 12, 12, 12)));
	}
	
}
