package zen.view.components;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;

public class WindowButton extends JButton {

	private static final long serialVersionUID = -571529778913437022L;

	public WindowButton(String name) {
		super(name);
		
		this.initializeDisplay();
	}
	
	private void initializeDisplay() {
		this.setForeground(Color.black);
		this.setOpaque(false);
		this.setContentAreaFilled(false);
		this.setFocusPainted(false);
		this.setAlignmentX(JButton.CENTER_ALIGNMENT);
		this.setAlignmentY(JButton.CENTER_ALIGNMENT);
	}
	
	public void setNewFont(int size, boolean bold) {
		if (size > 0) {
			if (bold) this.setFont(new Font("MONOSPACED", Font.BOLD, size));
			else this.setFont(new Font("MONOSPACED", Font.PLAIN, size));
		}
	}
	
	public void setNewBorder(int marginTop, int marginLeft, int marginBottom, int marginRight, Color color, int thickness) {
		if (marginTop > 0 && marginLeft > 0 && marginBottom > 0 && marginRight > 0 && color != null && thickness > 0)
			this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(color, thickness), 
				BorderFactory.createEmptyBorder(marginTop, marginLeft, marginBottom, marginRight)));
	}
	
}
