package zen.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import zen.model.Difficulty;
import zen.model.Game;
import zen.model.GameMode;
import zen.model.PawnColor;
import zen.model.Utils;
import zen.view.AboutWindow;
import zen.view.BoardPanel;
import zen.view.GameWindow;
import zen.view.Menu;
import zen.view.RulesWindow;
import zen.view.ScorePanel;

public class MyMenuListener implements ActionListener {

	private JFrame window;
	private Game game;
	private Menu menu;

	/**
	 * Initialize the game and menu attribute.
	 * @param window The window where the menu is
	 * @param menu The menu to listen.
	 * @param game The current Game object
	 */
	public MyMenuListener(JFrame window, Menu menu, Game game) {
		if (menu != null && game != null) {
			
			this.window = window;
			this.menu = menu;
			this.game = game;
			
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(this.menu.getNewMenuItem()))
			this.restartGame();
		else if (e.getSource().equals(this.menu.getOpenMenuItem()))
			this.openBackup();
		else if (e.getSource().equals(this.menu.getSaveMenuItem()))
			this.saveCurrentGame();
		else if (e.getSource().equals(this.menu.getExitMenuItem()))
			MyMenuListener.exitGame();
		else if (e.getSource().equals(this.menu.getRulesMenuItem()))
			MyMenuListener.showRules();
		else if (e.getSource().equals(this.menu.getAboutMenuItem()))
			this.showAbout();
	}
	
	/**
	 * Check if the player is currently in game
	 * @return True if the player is in game else False
	 */
	public boolean currentlyInGame() {
		return this.window instanceof GameWindow;
	}

	public void restartGame() {
		if (this.currentlyInGame()) {
			GameMode currentGameMode = this.game.getGameMode();
			Difficulty difficulty = null;
			if (currentGameMode == GameMode.VERSUSIA) difficulty = this.game.getIADifficulty();
			
			this.game.initGame();
			this.game.setGameMode(currentGameMode);
			if (difficulty != null) this.game.setIADifficulty(difficulty);
			
			((GameWindow) this.window).refreshWindow(this.game.getCurrentPlayer().toString(), this.game.getCurrentPlayer().getColor(),
					this.game.getNBPawnsLeft(PawnColor.WHITE), this.game.getNBPawnsLeft(PawnColor.BLACK));
			JOptionPane.showMessageDialog(null, "The game has been restarted.", "Restart",
			        JOptionPane.INFORMATION_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null, "You're not currently in game !", "Error",
			        JOptionPane.ERROR_MESSAGE);
		}
		
	}

	private void openBackup() {
	    JFileChooser jfc = new JFileChooser();
	    jfc.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
	    jfc.setDialogTitle("Load a backup");
	    jfc.setFileFilter(new FileNameExtensionFilter("Zen files", "zen"));
	    
		if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			String path = jfc.getSelectedFile().getAbsolutePath();
			
			this.game.loadGame(path);
			this.window.dispose();
			MyMenuListener.startGameWindow(this.game);
		}
	}

	private void saveCurrentGame() {
		if (this.currentlyInGame()) {
			JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
			jfc.setDialogTitle("Save the game");
			
			if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				String path = jfc.getSelectedFile().getAbsolutePath();
				
				if (!path.endsWith(".zen") && (!path.endsWith("/") || !path.endsWith("\\"))) path += ".zen";
				
				game.saveGame(path);
				JOptionPane.showMessageDialog(null, "Game saved in " + path, "Save",
				        JOptionPane.INFORMATION_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(null, "You're not currently in game !", "Error",
			        JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public static void startGameWindow(Game game) {
		if (game != null) {
			
			ScorePanel sc = new ScorePanel();
			BoardPanel boardPanel = new BoardPanel(game.getBoard(), sc);
			new GameWindow(game, boardPanel, sc);
			
		} else {
			JOptionPane.showMessageDialog(null, "Cannot load the game !", "Error",
			        JOptionPane.ERROR_MESSAGE);
		}
	}

	public static void exitGame() {
		int result = JOptionPane.showConfirmDialog(null, "Do you really want to leave?", "Quit the game", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

		if (result == JOptionPane.YES_OPTION) System.exit(0);
	}

	public static void showRules() {
		Utils utils = new Utils();
		new RulesWindow(utils.readRules());
	}

	private void showAbout() {
		new AboutWindow();
	}

}