package zen.controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

import zen.Main;
import zen.model.Game;
import zen.model.PawnColor;
import zen.model.Sound;
import zen.view.GameWindow;
import zen.view.ScorePanel;

public class ScorePanelListener extends MouseAdapter {
	
	private ScorePanel sc;
	private GameWindow gameWindow;
	private Game game;
	
	/**
	 * Initialize the attributes of the class.
	 * @param sc The ScorePanel object to listen on
	 * @param gameWindow The GameWindow object where the ScorePanel is.
	 * @param game The current Game instance.
	 */
	public ScorePanelListener(ScorePanel sc, GameWindow gameWindow, Game game) {
		this.sc = sc;
		this.gameWindow = gameWindow;
		this.game = game;
		
		this.sc.updateComponents(this.game.getCurrentPlayer().toString(), this.game.getCurrentPlayer().getColor(),
				this.game.getNBPawnsLeft(PawnColor.WHITE), this.game.getNBPawnsLeft(PawnColor.BLACK));
	}

	/**
	 * Run when you click on one of the two icons.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getSource().equals(this.sc.getHomeIcon())) {
			clickOnHome();
		} else if (e.getSource().equals(this.sc.getSoundIcon())) {
			clickOnSound();
		} else if (e.getSource().equals(this.sc.getRestartIcon())) {
			restartGame();
		}
	}
	
	/**
	 * Re launch the game
	 */
	private void clickOnHome() {
		int result = JOptionPane.showConfirmDialog(null, "Do you really want to go back ?", "Home", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
		
		if (result == JOptionPane.YES_OPTION && this.gameWindow != null) {
			this.gameWindow.dispose();
			Main.startGame(new Game());
		}
	}
	
	/**
	 * Enable / Disable the sound of the game.
	 */
	private void clickOnSound() {
		if (this.game.getSound() == Sound.ON) {
			
			this.sc.setSoundIcon(false);
			this.game.setSound(Sound.OFF);
			
		} else if (this.game.getSound() == Sound.OFF) {
			
			this.sc.setSoundIcon(true);
			this.game.setSound(Sound.ON);
		}
	}
	
	/**
	 * Restart the current game.
	 */
	private void restartGame() {
		this.gameWindow.getMenu().getListener().restartGame();
	}
	
}
