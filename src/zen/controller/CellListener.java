package zen.controller;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import zen.model.Game;
import zen.model.GameMode;
import zen.model.IA;
import zen.model.PawnColor;
import zen.model.Player;
import zen.model.SoundName;
import zen.view.CellPanel;
import zen.view.ScorePanel;

public class CellListener extends MouseAdapter {

	private final Color SELECTEDCOLOR = Color.green;
	private final Color POSSIBLEMOVECOLOR = Color.red;
	public final static Color DEFAULTCOLOR = Color.gray;
	
	private Game game;
	private ScorePanel sc;
	private CellPanel[][] cellPanel;
	
	/**
	 * Initialize all the attributes of the class.
	 * @param game The current game object
	 * @param cellPanel The arrays of CellPanel
	 * @param sc The ScorePanel
	 */
	public CellListener(Game game, CellPanel[][] cellPanel, ScorePanel sc) {
		this.game = game;
		this.cellPanel = cellPanel;
		this.sc = sc;
		
		this.refreshAllPanels();
	}

	/**
	 * Run when the mouse is clicked.
	 */
	@Override
	public void mouseClicked(MouseEvent event) {
		if (game.isRunning()) {
			CellPanel currentPanel = (CellPanel) event.getSource();
			this.selectPawn(currentPanel);
		}
	}
	
	/**
	 * Launch when you click on a CellPanel
	 * @param currentPanel The CellPanel clicked
	 */
	private void selectPawn(CellPanel currentPanel) {
		if (currentPanel != null) {
			if (currentPanel.getBorderColor() != POSSIBLEMOVECOLOR) {
				this.applyBorder(currentPanel);
				this.displaySelectable(currentPanel);
			} else {
				this.newMoveTo(currentPanel);
				this.resetBorder();
			}
		}
	
	}
	
	/**
	 * Return the coords of a specified CellPanel.
	 * @param toFind The specified CellPanel
	 * @return The coords of the CellPanel
	 */
	private int[] findCoordsOfPanel(CellPanel toFind) {
		for (int i = 0; i < Game.SIZE; i++) {
			for (int j = 0; j < Game.SIZE; j++) {
				if (this.cellPanel[i][j] == toFind) return new int[] {i, j};
			}
		}
		return null;
	}
	
	/**
	 * Find the pawn currently selected
	 * @return The CellPanel that represents the pawn currently selected
	 */
	private CellPanel findSelectedPanel() {
		for (CellPanel[] panels : this.cellPanel) {
			for (CellPanel panel : panels) {
				if (panel.getBorderColor() == SELECTEDCOLOR) return panel;
			}
		}
		return null;
	}
	
	private void resetBorder() {
		for (CellPanel[] panels : this.cellPanel) {
			for (CellPanel panel : panels) {
				panel.setNewBorder(DEFAULTCOLOR);
			}
		}
	}
	
	/**
	 * Apply the new border of the selected pawn
	 * @param panelBorder The pawn selected
	 */
	private void applyBorder(CellPanel panelBorder) {
		for (CellPanel[] panels : this.cellPanel) {
			for (CellPanel panel : panels) {
				panel.setNewBorder(DEFAULTCOLOR);
			}
		}
		
		int[] coords = this.findCoordsOfPanel(panelBorder);
		if (game.getCurrentPlayer().canSelect(coords[0], coords[1]))
			panelBorder.setNewBorder(SELECTEDCOLOR);
	}
	
	/**
	 * Display all the possible destination of the selected pawn
	 * @param currentPanel The CellPanel that represents the selected pawn
	 */
	private void displaySelectable(CellPanel currentPanel) {
		int[] currentCoords = this.findCoordsOfPanel(currentPanel);
		
		if (currentCoords != null && currentCoords.length == 2) {
			ArrayList<int[]> possibleMoves = game.getCurrentPlayer().possibleMoves(currentCoords[0], currentCoords[1]);
			if (possibleMoves != null) {
				for (int[] coords : possibleMoves) {
					this.cellPanel[coords[0]][coords[1]].setNewBorder(POSSIBLEMOVECOLOR);
				}
			}
		}
	}
	
	/**
	 * Check if it's the turn of the IA to play, if yes play
	 */
	private void checkIAandPlay() {
		if (this.game.getGameMode() == GameMode.VERSUSIA && this.game.getCurrentPlayer() instanceof IA) {
			((IA) this.game.getPlayer2()).newMove();
			this.checkWinner();
			this.game.changeCurrent();
		}
	}
	
	/**
	 * Refresh all the CellPanel
	 */
	public void refreshAllPanels() {
		this.checkIAandPlay();
		
		for (int i = 0; i < Game.SIZE; i++) {
			for (int j = 0; j < Game.SIZE; j++) {
				this.cellPanel[i][j].setColor(this.game.getBoard()[i][j].getColor());
				
				if ((i+j) % 2 == 0) this.cellPanel[i][j].updateComponents(true);
				else this.cellPanel[i][j].updateComponents(false);
			}
		}
	}
	
	/**
	 * Check if there's a winner, if yes show it
	 */
	private void checkWinner() {
		Player winner = this.game.isWinner();
		if (winner != null) {
			sc.setWinner(this.game.end(winner));
			this.game.playSound(SoundName.WINNER);
		}
	}
	
	/**
	 * Make a new Move
	 * @param panelMove The destination of the selected pawn
	 */
	private void newMoveTo(CellPanel panelMove) {
		CellPanel fromPanel = this.findSelectedPanel();
		int[] fromCoords = this.findCoordsOfPanel(fromPanel);
		int[] toCoords = this.findCoordsOfPanel(panelMove);
		
		if (fromCoords != null && fromCoords.length == 2 && toCoords != null && toCoords.length == 2) {
			if (this.game.getCurrentPlayer().movePawn(fromCoords[0], fromCoords[1], toCoords[0], toCoords[1])) {
				
				if (this.game.getBoard()[toCoords[0]][toCoords[1]].getColor() == zen.model.PawnColor.ZEN)
					this.game.playSound(SoundName.ZEN);
				else 
					this.game.playSound(SoundName.MOVE);
				
				this.checkWinner();
				this.game.changeCurrent();
				
				this.sc.updateComponents(this.game.getCurrentPlayer().toString(), this.game.getCurrentPlayer().getColor(),
						this.game.getNBPawnsLeft(PawnColor.WHITE), this.game.getNBPawnsLeft(PawnColor.BLACK));
				this.refreshAllPanels();
			}
		}

	}

}
