package zen.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import zen.model.Difficulty;
import zen.model.Game;
import zen.model.GameMode;
import zen.view.*;

public class FirstWindowListener implements ActionListener {

	private Game game;
	private FirstWindow window;

	/**
	 * Create the listener for the FirstWindow.
	 * @param window The FirstWindow instance to listen on
	 * @param game The current Game object
	 */
	public FirstWindowListener(FirstWindow window, Game game) {
		if (window != null && game != null) {
			this.window = window;
			this.game = game;
		} else {
			System.err.println("Error FirstWindowListener(window, game): Params can't be null !");
		}			
	}

	/**
	 * Call when the player make an action on the window
	 * @param action The action of the player
	 */
	public void actionPerformed(ActionEvent action) {
		if (action.getSource().equals(this.window.getPlayWithIA()))
			this.playWithIA();
		else if (action.getSource().equals(this.window.getPlayWithPlayer()))
			this.playWithPlayer();
		else if (action.getSource().equals(this.window.getShowRulesBtn()))
			this.showRules();
		else if (action.getSource().equals(this.window.getExitBtn()))
			this.exitGame();
	}

	/**
	 * Create the GameModeWindow to select a game mode.
	 */
	private void playWithIA() {
		String[] difficulties = new String[] {Difficulty.EASY.toString(), Difficulty.NORMAL.toString(), Difficulty.HARD.toString()};
	    
		int response = JOptionPane.showOptionDialog(null, "Choose the IA difficulty", "Difficulty",
	        JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
	        null, difficulties, difficulties[0]);
	    
	    if (response == 0 || response == 1 || response == 2) {
			this.window.dispose();
	    	this.game.setGameMode(GameMode.VERSUSIA);
	    	this.game.initGame();
	    	
		    if (response == 0) {
		    	this.game.setIADifficulty(Difficulty.EASY);
		    } else if (response == 1) {
		    	this.game.setIADifficulty(Difficulty.NORMAL);
		    } else if (response == 2) {
		    	this.game.setIADifficulty(Difficulty.HARD);
		    }
		    MyMenuListener.startGameWindow(game);
	    }
	}
	
	/**
	 * Create the GameModeWindow to select a game mode.
	 */
	private void playWithPlayer() {
		this.window.dispose();
		
		this.game.setGameMode(GameMode.VERSUSHUMAN);
		this.game.initGame();
		MyMenuListener.startGameWindow(game);
	}

	/**
	 * Create the RulesWindow to show the rules.
	 */
	private void showRules() {
		MyMenuListener.showRules();
	}

	/**
	 * Exit the Game.
	 */
	private void exitGame() {
		MyMenuListener.exitGame();
	}

}