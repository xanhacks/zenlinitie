package zen.model;

import java.io.Serializable;

public class Cell implements Serializable {

	private static final long serialVersionUID = -4705463964908898674L;
	
	private PawnColor color;
	
	/**
	 * Initialize the color attribute.
	 * @param color The value of the color attribute
	 */
	public Cell(PawnColor color) {
		if (color != null) {
			this.color = color;
		} else {
			System.err.println("Error Case(color): The param can't be null !");
		}
	}
	
	/**
	 * Getter for color
	 * @return The color attribute
	 */
	public PawnColor getColor() {
		return this.color;
	}
	
	/**
	 * Set the color of the case
	 * @param color The new color
	 */
	public void setColor(PawnColor color) {
		this.color = color;
	}
	
	/**
	 * The way to represent the case.
	 * @return The string which represent the case
	 */
	public String toString() {
		String ret = null;
		
		if (this.color != PawnColor.VOID)
			ret = String.valueOf(this.color.toString().charAt(0));
		else
			ret = "·";
		
		return ret;
	}
	
}
