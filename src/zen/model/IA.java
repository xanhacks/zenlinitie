package zen.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.TreeMap;

public class IA extends Player {

	private static final long serialVersionUID = 8313045426433490355L;

	private Difficulty difficulty;

	/**
	 * Create an IA player with a specific color.
	 * @param color The color of the player
	 * @param board Board of the game associated with the player
	 */
	public IA(PawnColor color, Cell[][] board) {
		super(color, board);

	}
	
	public Difficulty getDifficulty() {
		return this.difficulty;
	}
	
	/**
	 * Set the difficulty of the IA
	 * @param difficulty
	 */
	public void setDifficulty(Difficulty difficulty) {
		if (difficulty != null)
			this.difficulty = difficulty;
		else
			System.err.println("Error setDifficulty(difficulty): The difficulty can't be null !");
	}

	/**
	 * Process a move that depends on the difficulty.
	 */
	public void newMove() {
		if (difficulty == Difficulty.EASY)
			this.easyMove();
		else if (difficulty == Difficulty.NORMAL)
			this.normalMove();
		else if (difficulty == Difficulty.HARD)
			this.hardMove();
	}
	
	/**
	 * Process a new move with the difficulty set to easy.
	 * The IA plays randomly.
	 */
	private void easyMove() {
		boolean isMove = false;
		Random rand = new Random();
		int[] move = null;
		int selectCoordX = -1;
		int selectCoordY = -1;
		
		while(!this.canSelect(selectCoordX, selectCoordY)) {
			selectCoordX = rand.nextInt(Game.SIZE);
			selectCoordY = rand.nextInt(Game.SIZE);
		}
		
		ArrayList<int[]> moves = this.possibleMoves(selectCoordX, selectCoordY);
		if (moves != null && moves.size() > 0) {
			move = moves.get(rand.nextInt(moves.size()));
			
			if (move != null && move.length == 2) {
				isMove = this.movePawn(selectCoordX, selectCoordY, move[0], move[1]);
			}
		}
			
		if (isMove) System.out.println("[IA] Pawn: [" + this.coordsToInput(selectCoordX, selectCoordY) + "] to [" + this.coordsToInput(move[0], move[1]) + "]");
		else this.easyMove();
	}
	
	/**
	 * Process a new move with the difficulty set to normal.
	 */
	private void normalMove() {
		boolean isMove = false;
		int[] mostFar = this.mostFarFromCenter();
		int fromX = -1;
		int fromY = -1;
		int newX = -1;
		int newY = -1;
		
		if (mostFar != null && mostFar.length == 2) {
			fromX = mostFar[0];
			fromY = mostFar[1];
			
			int[] bestMove = this.nearestMoveCenter(mostFar);
			
			if (bestMove != null && bestMove.length == 2) {
				newX = bestMove[0];
				newY = bestMove[1];
				
				isMove = this.movePawn(fromX, fromY, newX, newY);
			}
		}
		
		if (isMove) System.out.println("[IA] Pawn: [" + this.coordsToInput(fromX, fromY) + "] to [" + this.coordsToInput(newX, newY) + "]");
		else this.easyMove();
	}
	
	/**
	 * Process a new move with the difficulty set to hard.
	 */
	private void hardMove() {
		boolean isMove = false;
		ArrayList<int[]> allPawns = this.getAllPawns(this.getColor());
		int fromX = -1;
		int fromY = -1;
		int toX = -1;
		int toY = -1;
		
		// TreeMap -> Score : fromX, fromY, toX, toY
		TreeMap<Double, int[]> distCoordFromDest = new TreeMap<Double, int[]>();
		
		int[] centerCoords = this.findCenter();
		
		if (allPawns != null && centerCoords != null) {
			int centerX = centerCoords[0];
			int centerY = centerCoords[1];
			
			for (int[] pawnCoords : allPawns) {
				fromX = pawnCoords[0];
				fromY = pawnCoords[1];

				double distFromCenter = this.distanceBetween(fromX, fromY, centerX, centerY);
				
				for (int[] moves : this.possibleMoves(fromX, fromY)) {
					toX = moves[0];
					toY = moves[1];

					double futureDistFromCenter = this.distanceBetween(toX, toY, centerX, centerY);
					distCoordFromDest.put(futureDistFromCenter-distFromCenter, new int[] {fromX, fromY, toX, toY});
				}
			}
		}
		
		// Retrieve the best score
		Iterator<Double> it = distCoordFromDest.keySet().iterator();
		if (it.hasNext()) {
			Double key = it.next();
			int[] moveFound = distCoordFromDest.get(key);
			
			if (moveFound != null && moveFound.length == 4) {
				fromX = moveFound[0];
				fromY = moveFound[1];
				toX = moveFound[2];
				toY = moveFound[3];
				
				isMove = this.movePawn(fromX, fromY, toX, toY);
			}
				
		}
		
		if (isMove) System.out.println("[IA] Pawn: [" + this.coordsToInput(fromX, fromY) + "] to [" + this.coordsToInput(toX, toY) + "]");
		else this.normalMove();
	}
	
	/**
	 * Return the coordinates of all the pawns of a specific color
	 * @param pawnColor The specific color
	 * @return Array list of coordinates of all the pawns
	 */
	private ArrayList<int[]> getAllPawns(PawnColor pawnColor) {
		ArrayList<int[]> allPawns = new ArrayList<int[]>();
		
		for (int i = 0; i < Game.SIZE; i++) {
			for (int j = 0; j < Game.SIZE; j++) {
				if (this.board[i][j].getColor() == pawnColor)
					allPawns.add(new int[] {i, j});
			}
		}
		
		return allPawns;
	}
	
	/**
	 * Return the nearest move from center of a pawn.
	 * @param pawnToMove The coords of the specified pawn
	 * @return The best move for a pawn to make
	 */
	private int[] nearestMoveCenter(int[] pawnToMove) {
		int[] ret = null;
		
		if (pawnToMove != null && pawnToMove.length == 2) {
			ArrayList<int[]> possibleMoves = this.possibleMoves(pawnToMove[0], pawnToMove[1]);
			double max = -1;
			
			for (int[] move : possibleMoves) {
				double dist = this.distanceBetween(pawnToMove[0], pawnToMove[0], move[0], move[1]);
				
				if (dist > max) {
					dist = max;
					ret = move;
				}
			}
			
		}
		
		return ret;
	}
	
	/**
	 * Return the distance between to coords
	 * @param fromX X coord from the first pawn
	 * @param fromY Y coord from the first pawn
	 * @param toX X coord from the second pawn
	 * @param toY Y coord from the second pawn
	 * @return The distance between this 2 pawns.
	 */
	private double distanceBetween(int fromX, int fromY, int toX, int toY) {
		return Math.sqrt(Math.pow(fromX-toX, 2) + Math.pow(fromY-toY, 2));
	}
	
	/**
	 * Return the coordinates of the pawn that is the farthest of the center of all the pawns.
	 * @return The coordinates of the farthest pawn.
	 */
	private int[] mostFarFromCenter() {
		double maxDistance = -1;
		int[] coordsMax = null;
		int[] center = this.findCenter();
		
		for (int x = 0; x < Game.SIZE; x++) {
			for (int y = 0; y < Game.SIZE; y++) {
				if (this.board[x][y].getColor() == this.getColor() || this.board[x][y].getColor() == PawnColor.ZEN) {
					double dist = this.distanceBetween(x, y, center[0], center[1]);
					
					if (dist > maxDistance && this.possibleMoves(x, y) != null) {
						maxDistance = dist;
						coordsMax = new int[] {x, y};
					}
				}
			}
		}

		return coordsMax;
	}
	
	/**
	 * Find the center of all the IA pawns.
	 * @return The coords of the center
	 */
	private int[] findCenter() {
		int pawnNumber = 0;
		int centerX = 0;
		int centerY = 0;
		
		for (int x = 0; x < Game.SIZE; x++) {
			for (int y = 0; y < Game.SIZE; y++) {
				if (this.board[x][y].getColor() == this.getColor() || this.board[x][y].getColor() == PawnColor.ZEN) {
					centerX += x;
					centerY += y;
					pawnNumber++;
				}
			}
		}
		if (pawnNumber > 0) {
			centerX /= pawnNumber;
			centerY /= pawnNumber;
		} else {
			centerX = -1;
			centerY = -1;
		}
		
		return new int[] {centerX, centerY};
	}

}