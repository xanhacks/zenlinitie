package zen.model;

public class Human extends Player {

	private static final long serialVersionUID = -1477890982749564234L;

	/**
	 * Create a Human player.
	 * @param color Color of the player
	 * @param board Board of the game associated with the player
	 */
	public Human(PawnColor color, Cell[][] board) {
		super(color, board);
	}

}