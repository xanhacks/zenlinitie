package zen.model;

public enum PawnColor {
	WHITE,
	BLACK,
	ZEN,
	VOID;
}
