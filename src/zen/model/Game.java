package zen.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Game implements Serializable {

	private static final long serialVersionUID = -461074913114500875L;

	public static int SIZE = 11;

	private Player player1;
	private Player currentPlayer;
	private Player player2;

	private GameMode gameMode;
	private Sound sound;
	private boolean running;
	private Cell[][] board;

	/**
	 * Initialize all the attributes to his default value.
	 */
	public Game() {
		this.player1 = null;
		this.player2 = null;
		this.currentPlayer = null;
		this.gameMode = null;
		this.sound = null;
		this.board = null;
		this.running = false;
	}

	public Player getPlayer1() {
		return this.player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getCurrentPlayer() {
		return this.currentPlayer;
	}

	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public Player getPlayer2() {
		return this.player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}

	public GameMode getGameMode() {
		return this.gameMode;
	}

	public void setGameMode(GameMode gameMode) {
		this.gameMode = gameMode;
	}

	public boolean isRunning() {
		return this.running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public Cell[][] getBoard() {
		return this.board;
	}

	public void setBoard(Cell[][] board) {
		this.board = board;
	}
	
	public Player getOpponentPlayer() {
		if (this.currentPlayer == this.player1)
			return this.player2;
		return this.player1;
	}
	
	public Sound getSound() {
		return this.sound;
	}
	
	public void setSound(Sound sound) {
		this.sound = sound;
	}

	/**
	 * End the game.
	 */
	public String end(Player player) {
		this.running = false;
		
		if (player != null) {
			if (player.getColor() == PawnColor.VOID)
				return new String("Draw");
			else
				return new String(player.toString());
		}
		return null;
	}

	/**
	 * Check if there is a winner.
	 * @return True if they're a winner else false.
	 */
	public Player isWinner() {
		Player ret = null;
		
		int maxBlack = this.getNBPawnsLeft(PawnColor.BLACK) + this.getNBPawnsLeft(PawnColor.ZEN);
		if (this.recursiveWinner(this.getFirstCell(PawnColor.BLACK), null, maxBlack, PawnColor.BLACK)) {
			if (this.currentPlayer.getColor() == PawnColor.BLACK)
				ret = this.currentPlayer;
			else
				ret = this.getOpponentPlayer();
		}

		
		int maxWhite = this.getNBPawnsLeft(PawnColor.WHITE) + this.getNBPawnsLeft(PawnColor.ZEN);
		if (this.recursiveWinner(this.getFirstCell(PawnColor.WHITE), null, maxWhite, PawnColor.WHITE)) {
			if (ret == null) {
				if (this.currentPlayer.getColor() == PawnColor.WHITE)
					ret = this.currentPlayer;
				else
					ret = this.getOpponentPlayer();
			} else { // 2 players won !
				ret = new Human(PawnColor.VOID, null);
			}
		}
		
		return ret;
	}
	
	/**
	 * Check recursively if there is a winner.
	 * @param coords Current coords to check
	 * @param markedPawns Array of pawns already checked
	 * @param max Number of pawn of the player
	 * @return True if there is a winner else false.
	 */
	private boolean recursiveWinner(int[] coords, ArrayList<int[]> markedPawns, int max, PawnColor color) {
		boolean ret = false;
		
		if (markedPawns == null) markedPawns = new ArrayList<int[]>();
		if (max-1 == markedPawns.size()) ret = true;
		
		if (!ret && coords != null) {
			if (this.noCoordsIn(markedPawns, coords[0], coords[1])) {
				markedPawns.add(new int[] {coords[0], coords[1]});
			}
			
			ArrayList<int[]> arround = this.isPawnAround(coords[0], coords[1], color);
			for (int[] coord : arround) {
				if (this.noCoordsIn(markedPawns, coord[0], coord[1]))
					ret = this.recursiveWinner(coord, markedPawns, max, color);
			}
		}

		return ret;
	}
	
	/**
	 * Check if coordinates are in array of coordinates.
	 * @param coords Array of coordinates
	 * @param coordX X coordinate to check
	 * @param coordY Y coordinate to check
	 * @return True if the coordinates to check are in the array else false.
	 */
	private boolean noCoordsIn(ArrayList<int[]> coords, int coordX, int coordY) {
		if (coords != null) {
			for (int[] coord : coords)
				if (coord[0] == coordX && coord[1] == coordY) return false;
		}
		return true;
	}
	
	/**
	 * Check if they're pawns around this coords.
	 * @param coordX X coordinate of the pawn.
	 * @param coordY Y coordinate of the pawn.
	 * @return Return the coordinate of the pawn around
	 */
	private ArrayList<int[]> isPawnAround(int coordX, int coordY, PawnColor color) {
		ArrayList<int[]> ret = null;
		
		if (this.currentPlayer.validCoords(coordX, coordY)) {
			ret = new ArrayList<int[]>();
			
			int[][] coordsToCheck = {
					{coordX-1, coordY-1},
					{coordX-1, coordY},
					{coordX-1, coordY+1},
					{coordX, coordY+1},
					{coordX+1, coordY+1},
					{coordX+1, coordY},
					{coordX+1, coordY-1},
					{coordX, coordY-1},
			};
			
			for (int[] coords : coordsToCheck) {
				if (this.currentPlayer.validCoords(coords[0], coords[1])) {
					if (this.board[coords[0]][coords[1]].getColor() == color || this.board[coords[0]][coords[1]].getColor() == PawnColor.ZEN)
						ret.add(new int[] {coords[0], coords[1]});
				}
			}
		}

		return ret;
	}
	
	/**
	 * Get the first occurrence of a cell with a specific color
	 * @param color The specific color
	 * @return The first occurrence if found, else null
	 */
	private int[] getFirstCell(PawnColor color) {
		for (int i = 0; i < Game.SIZE; i++) {
			for (int j = 0; j < Game.SIZE; j++) {
				if (this.board[i][j].getColor() == color) {
					return new int[] {i, j};
				}
			}
		}
		return null;
	}
	
	/**
	 * Set the IA difficulty.
	 * @param difficulty The new difficulty of the IA.
	 */
	public void setIADifficulty(Difficulty difficulty) {
		if (difficulty != null && this.gameMode == GameMode.VERSUSIA) {
			if (this.player2 instanceof IA) {
				((IA) this.player2).setDifficulty(difficulty);
			}
		}
	}
	
	/**
	 * Return the difficulty of the IA
	 * @return The difficulty of the IA
	 */
	public Difficulty getIADifficulty() {
		if (this.gameMode == GameMode.VERSUSIA && this.player2 instanceof IA)
			return ((IA) this.player2).getDifficulty();
		return null;
	}

	/**
	 * Initialize attributes (board, players, ...) of the class Game.
	 */
	public void initGame() {
		this.running = true;
		this.sound = Sound.ON;
		this.initBoard();
		
		if (this.gameMode == GameMode.VERSUSHUMAN) {
			this.player1 = new Human(PawnColor.WHITE, this.board);
			this.player2 = new Human(PawnColor.BLACK, this.board);
		} else if (this.gameMode == GameMode.VERSUSIA) {
			this.player1 = new Human(PawnColor.WHITE, this.board);
			this.player2 = new IA(PawnColor.BLACK, this.board);
		}
		
		this.selectRandomPlayer();
	}

	/**
	 * Change the current player to the other player.
	 */
	public void changeCurrent() {
		if (this.currentPlayer == this.player1)
			this.currentPlayer = this.player2;
		else
			this.currentPlayer = this.player1;
	}

	/**
	 * Select a random player to start the game.
	 */
	private void selectRandomPlayer() {
		Player[] players = {this.player1, this.player2};
		
		Random rand = new Random();
		this.currentPlayer = players[rand.nextInt(players.length)];
	}
	
	/**
	 * Return the number of pawns alive.
	 * @param color Color of pawns to check
	 * @return the number of white pawn still alive.
	 */
	public int getNBPawnsLeft(PawnColor color) {
		int ret = 0;
		
		for (Cell[] line : this.board) {
			for (Cell cell : line) {
				if (cell.getColor().equals(color)) ret++;
			}
		}
		
		return ret;
	}

	/**
	 * Create the default board for the game.
	 */
	private void initBoard() {
		this.board = new Cell[Game.SIZE][Game.SIZE];
		
		for (int i = 0; i < Game.SIZE; i++) {
			for (int j = 0; j < Game.SIZE; j++) {
				this.board[i][j] = new Cell(PawnColor.VOID);
			}
		}

		// zen
		this.board[5][5] = new Cell(PawnColor.ZEN);

		// angle
		this.board[0][0] = new Cell(PawnColor.WHITE);
		this.board[Game.SIZE-1][0] = new Cell(PawnColor.BLACK);
		this.board[0][Game.SIZE-1] = new Cell(PawnColor.BLACK);
		this.board[Game.SIZE-1][Game.SIZE-1] = new Cell(PawnColor.WHITE);

		// square
		this.board[0][5] = new Cell(PawnColor.BLACK);
		this.board[1][4] = new Cell(PawnColor.WHITE);
		this.board[1][6] = new Cell(PawnColor.WHITE);
		this.board[2][3] = new Cell(PawnColor.BLACK);
		this.board[2][7] = new Cell(PawnColor.BLACK);
		this.board[3][2] = new Cell(PawnColor.WHITE);
		this.board[3][8] = new Cell(PawnColor.WHITE);
		this.board[4][1] = new Cell(PawnColor.BLACK);
		this.board[4][9] = new Cell(PawnColor.BLACK);
		this.board[5][0] = new Cell(PawnColor.WHITE);
		this.board[5][10] = new Cell(PawnColor.WHITE);
		this.board[6][1] = new Cell(PawnColor.BLACK);
		this.board[6][9] = new Cell(PawnColor.BLACK);
		this.board[7][2] = new Cell(PawnColor.WHITE);
		this.board[7][8] = new Cell(PawnColor.WHITE);
		this.board[8][3] = new Cell(PawnColor.BLACK);
		this.board[8][7] = new Cell(PawnColor.BLACK);
		this.board[9][4] = new Cell(PawnColor.WHITE);
		this.board[9][6] = new Cell(PawnColor.WHITE);
		this.board[10][5] = new Cell(PawnColor.BLACK);
	}
	
	/**
	 * Load the game object
	 * @param gamePath The file to load
	 * @return true if everything is good else false
	 */
	public boolean loadGame(String gamePath) {
		boolean loaded = false;
		
		if (gamePath != null && !gamePath.isEmpty()) {
			Game game = null;
			try {
				FileInputStream fileIn = new FileInputStream(gamePath);
				ObjectInputStream in = new ObjectInputStream(fileIn);
				
				game = (Game) in.readObject();
				
				in.close();
				fileIn.close();
			} catch (IOException err) {
				System.out.println("Error loadGame(gamePath): Cannot open the file at ".concat(gamePath));
			} catch (ClassNotFoundException err) {
				System.out.println("Error loadGame(gamePath): Error while extracting objects.");
			}
	
			if (game != null) {
				this.setPlayer1(game.getPlayer1());
				this.setPlayer2(game.getPlayer2());
				this.setCurrentPlayer(game.getCurrentPlayer());
				this.setGameMode(game.getGameMode());
				this.setSound(game.getSound());
				this.setRunning(game.isRunning());
				this.setBoard(game.getBoard());
				System.out.println("[*] Game loaded.");
				loaded = true;
			}
		}
		
		return loaded;
	}
	
	/**
	 * Save this object into a file.
	 * @param savePath The path of the file.
	 */
	public void saveGame(String savePath) {
		if (savePath != null && !savePath.isEmpty()) {
			try {
				FileOutputStream fileOut = new FileOutputStream(savePath);
				ObjectOutputStream out = new ObjectOutputStream(fileOut);

				out.writeObject(this);

				out.close();
				fileOut.close();
				
				System.out.println("[*] Game saved in ".concat(savePath));
			} catch (IOException i) {
				i.printStackTrace();
			}
		}
	}
	
	public void playSound(SoundName soundName) {
		if (this.getSound() == Sound.ON) {
		    URL url = null;
		    
		    if (soundName == SoundName.WINNER)
		    	url = getClass().getClassLoader().getResource(PathConfig.WINNER_SONG_PATH);
		    else if (soundName == SoundName.MOVE)
		    	url = getClass().getClassLoader().getResource(PathConfig.MOVE_SONG_PATH);
		    else if (soundName == SoundName.ZEN)
		    	url = getClass().getClassLoader().getResource(PathConfig.ZEN_SONG_PATH);
		    
		    if (url != null) {
				try {
					AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
					Clip clip = AudioSystem.getClip();
				    clip.open(audioIn);
				    clip.start();
				} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
					System.err.println("Error playSound(soundName): Error while playing this sound !");
				}
		    }
		}

	}

}