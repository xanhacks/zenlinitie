package zen.model;

public enum Difficulty {
	EASY,
	NORMAL,
	HARD
}