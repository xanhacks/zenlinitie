package zen.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class Player implements Serializable {

	private static final long serialVersionUID = 5096821685686285087L;

	private PawnColor color;
	protected Cell[][] board;
	private static int[] oldZenCoords;
	
	/**
	 * Associates a player with a color
	 * @param color The color of the player
	 * @param board Board of the game associated with the player
	 */
	public Player (PawnColor color, Cell[][] board) {
		if ((color == PawnColor.WHITE || color == PawnColor.BLACK) && board != null) {
			
			this.board = board;
			this.color = color;
			Player.oldZenCoords = null;
			
		} else {
			System.err.println("Error Player(color, board): The color can only be black or white, and the board not null !");
		}
	}

	/**
	 * Check if the coord are valid.
	 * @param coordX The X coordinates.
	 * @param coordY The Y coordinates.
	 * @return True if the coords are valid else false
	 */
	public boolean validCoords(int coordX, int coordY) {
		if (coordX >= 0 && coordX < Game.SIZE && coordY >= 0 && coordY < Game.SIZE)
			return true;
		return false;
	}
	
	/**
	 * Check if a player can select this case.
	 * @param coordX The X coordinates of the case.
	 * @param coordY The Y coordinates of the case.
	 * @return Return true if the player can select it else false
	 */
	public boolean canSelect(int coordX, int coordY) {
		boolean ret = false;
		
		if (this.validCoords(coordX, coordY)) {
			if (this.board[coordX][coordY].getColor() == this.getColor() || this.board[coordX][coordY].getColor() == PawnColor.ZEN) {
				ret = true;
			}
		}

		return ret;
	}

	/**
	 * Return the number of pawns in the vertical
	 * @param coordY The Y coordinates of the column to check.
	 * @return The number of pawns.
	 */
	private int numberPawnVertical(int coordY) {
		int number = 0;
		
		if (coordY >= 0 && coordY < Game.SIZE) {
			for(int i = 0; i < Game.SIZE; i++) {
				
				if (this.board[i][coordY].getColor() != PawnColor.VOID) number++;
				
			}
		}
		
		return number;
	}

	/**
	 * Return the number of pawns in the vertical
	 * @param coordX The X coordinates of the line to check.
	 * @return The number of pawns.
	 */
	private int numberPawnHorizontal(int coordX) {
		int number = 0;
		
		if (coordX >= 0 && coordX < Game.SIZE) {
			for(int i = 0; i < Game.SIZE; i++) {
				
				if (this.board[coordX][i].getColor() != PawnColor.VOID) number++;
				
			}
		}
		
		return number;
	}

	/**
	 * Number of pawn in the diagonal from top left to bottom right.
	 * @param coordX The X coord of the case on the diag to check.
	 * @param coordY The Y coord of the case on the diag to check.
	 * @return The number of pawns.
	 */
	private int numberPawnDiagLeftToRight(int coordX, int coordY) {
		int number = 0;
		
		if (this.validCoords(coordX, coordY)) {
			for (int i = 0; i < Game.SIZE; i++) {
				if (this.validCoords(coordX-i, coordY-i) && this.board[coordX-i][coordY-i].getColor() != PawnColor.VOID)
					number++;
				
				// i != 0 to not count the same position twice
				if (i != 0 && this.validCoords(coordX+i, coordY+i) && this.board[coordX+i][coordY+i].getColor() != PawnColor.VOID)
					number++;
				
			}
		}

		return number;
	}

	/**
	 * Number of pawn in the diagonal from top right to bottom left.
	 * @param coordX The X coord of the case on the diag to check.
	 * @param coordY The Y coord of the case on the diag to check.
	 * @return The number of pawns.
	 */
	private int numberPawnDiagRightToLeft(int coordX, int coordY) {
		int number = 0;
		
		if (this.validCoords(coordX, coordY)) {
			for (int i = 0; i < Game.SIZE; i++) {
				if (this.validCoords(coordX-i, coordY+i) && this.board[coordX-i][coordY+i].getColor() != PawnColor.VOID)
					number++;
				
				// i != 0 to not count the same position twice
				if (i != 0 && this.validCoords(coordX+i, coordY-i) && this.board[coordX+i][coordY-i].getColor() != PawnColor.VOID)
					number++;
				
			}
		}

		return number;
	}
	
	/**
	 * Check if they're pawns around this coords.
	 * @param coordX X coordinate of the pawn.
	 * @param coordY Y coordinate of the pawn.
	 * @return If yes return true else false
	 */
	private boolean isPawnAroundZen(int coordX, int coordY) {
		if (this.validCoords(coordX, coordY) && this.board[coordX][coordY].getColor() == PawnColor.ZEN) {

			int[][] coordsToCheck = {
					{coordX-1, coordY-1},
					{coordX-1, coordY},
					{coordX-1, coordY+1},
					{coordX, coordY+1},
					{coordX+1, coordY+1},
					{coordX+1, coordY},
					{coordX+1, coordY-1},
					{coordX, coordY-1},
			};
			
			for (int[] coords : coordsToCheck) {
				if (this.validCoords(coords[0], coords[1])) {
					if (this.board[coords[0]][coords[1]].getColor() != PawnColor.VOID)
						return true;
				}
			}
		}

		return false;
	}

	/**
	 * Check if a pawn can move
	 * @param fromX Actual Y coordinates of the pawn.
	 * @param fromY Actual Y coordinates of the pawn.
	 * @param newX The new X coordinates.
	 * @param newY The new Y coordinates.
	 * @return True if it can move else false
	 */
	private boolean pawnCanMove(int fromX, int fromY, int newX, int newY) {
		if (this.validCoords(fromX, fromY) && this.validCoords(newX, newY)) {
			ArrayList<int[]> possibleMoves = this.possibleMoves(fromX, fromY);
			
			if (possibleMoves != null) {
				for (int[] possibleMove : possibleMoves) {
					if (possibleMove[0] == newX && possibleMove[1] == newY)
						return true;
				}
			}
		
		}
		return false;
	}
	
	/**
	 * Get the first occurrence of a cell with a specific color
	 * @return The first occurrence if found, else null
	 */
	protected int[] getFirstCell() {
		for (int i = 0; i < Game.SIZE; i++) {
			for (int j = 0; j < Game.SIZE; j++) {
				if (this.board[i][j].getColor() == this.getColor()) {
					return new int[] {i, j};
				}
			}
		}
		return null;
	}

	/**
	 * Check if you can move the pawn, if yes move it.
	 * @param fromX Actual Y coordinates of the pawn.
	 * @param fromY Actual Y coordinates of the pawn.
	 * @param newX The new X coordinates.
	 * @param newY The new Y coordinates.
	 * @return True if the pawn has been move else false
	 */
	public boolean movePawn(int fromX, int fromY, int newX, int newY) {
		boolean move = false;
		
		if (this.pawnCanMove(fromX, fromY, newX, newY)) {
			this.board[newX][newY].setColor(this.board[fromX][fromY].getColor());
			this.board[fromX][fromY].setColor(PawnColor.VOID);
			
			if (this.board[newX][newY].getColor() == PawnColor.ZEN)
				Player.oldZenCoords = new int[] {fromX, fromY};
			else
				Player.oldZenCoords = null;

			move = true;
		}
		
		return move;
	}
	
	/**
	 * Return all the possibles moves that a pawn can do.
	 * @param coordX The X coordinate of the pawn.
	 * @param coordY The Y coordinate of the pawn.
	 * @return Array of possible case to go.
	 */
	public ArrayList<int[]> possibleMoves(int coordX, int coordY) {
		ArrayList<int[]> ret = new ArrayList<int[]>();
		
		if (this.validCoords(coordX, coordY)) {
			PawnColor pawnColor = this.board[coordX][coordY].getColor();

			if (pawnColor != PawnColor.VOID && this.canSelect(coordX, coordY)) {
				if (pawnColor == PawnColor.ZEN && !this.isPawnAroundZen(coordX, coordY)) {
					return ret;
				}

				// up
				int nbPawn = this.numberPawnVertical(coordY);
				boolean can = true;
				int i = 1;

				if (this.validCoords(coordX-nbPawn, coordY) && this.board[coordX-nbPawn][coordY].getColor() != this.getColor()) {
					while (can && i < nbPawn) {
						if (this.validCoords(coordX-i, coordY) && this.board[coordX-i][coordY].getColor() == this.getOpponentColor())
							can = false;
						i++;
					}
					if (can) ret.add(new int[] {coordX-nbPawn, coordY});
				}
				
				// down
				can = true;
				i = 1;
				
				if (this.validCoords(coordX+nbPawn, coordY) && this.board[coordX+nbPawn][coordY].getColor() != this.getColor()) {
					while (can && i < nbPawn) {
						if (this.validCoords(coordX+i, coordY) && this.board[coordX+i][coordY].getColor() == this.getOpponentColor())
							can = false;
						i++;
					}
					if (can) ret.add(new int[] {coordX+nbPawn, coordY});
				}
				
				// left
				nbPawn = this.numberPawnHorizontal(coordX);
				can = true;
				i = 1;
				
				if (this.validCoords(coordX, coordY-nbPawn) && this.board[coordX][coordY-nbPawn].getColor() != this.getColor()) {
					while (can && i < nbPawn) {
						if (this.validCoords(coordX, coordY-i) && this.board[coordX][coordY-i].getColor() == this.getOpponentColor())
							can = false;
						i++;
					}
					if (can) ret.add(new int[] {coordX, coordY-nbPawn});
				}
				
				// right
				can = true;
				i = 1;
				
				if (this.validCoords(coordX, coordY+nbPawn) && this.board[coordX][coordY+nbPawn].getColor() != this.getColor()) {
					while (can && i < nbPawn) {
						if (this.validCoords(coordX, coordY+i) && this.board[coordX][coordY+i].getColor() == this.getOpponentColor())
							can = false;
						i++;
					}
					if (can) ret.add(new int[] {coordX, coordY+nbPawn});
				}
				
				// diag top left
				nbPawn = this.numberPawnDiagLeftToRight(coordX, coordY);
				can = true;
				i = 1;
				
				if (this.validCoords(coordX-nbPawn, coordY-nbPawn) && this.board[coordX-nbPawn][coordY-nbPawn].getColor() != this.getColor()) {
					while (can && i < nbPawn) {
						if (this.validCoords(coordX-i, coordY-i) && this.board[coordX-i][coordY-i].getColor() == this.getOpponentColor())
							can = false;
						i++;
					}
					if (can) ret.add(new int[] {coordX-nbPawn, coordY-nbPawn});
				}
				
				// diag bottom right
				nbPawn = this.numberPawnDiagLeftToRight(coordX, coordY);
				can = true;
				i = 1;
				
				if (this.validCoords(coordX+nbPawn, coordY+nbPawn) && this.board[coordX+nbPawn][coordY+nbPawn].getColor() != this.getColor()) {
					while (can && i < nbPawn) {
						if (this.validCoords(coordX+i, coordY+i) && this.board[coordX+i][coordY+i].getColor() == this.getOpponentColor())
							can = false;
						i++;
					}
					if (can) ret.add(new int[] {coordX+nbPawn, coordY+nbPawn});
				}
				
				// diag top right
				nbPawn = this.numberPawnDiagRightToLeft(coordX, coordY);
				can = true;
				i = 1;
				
				if (this.validCoords(coordX-nbPawn, coordY+nbPawn) && this.board[coordX-nbPawn][coordY+nbPawn].getColor() != this.getColor()) {
					while (can && i < nbPawn) {
						if (this.validCoords(coordX-i, coordY+i) && this.board[coordX-i][coordY+i].getColor() == this.getOpponentColor())
							can = false;
						i++;
					}
					if (can) ret.add(new int[] {coordX-nbPawn, coordY+nbPawn});
				}
				
				// diag bot left
				nbPawn = this.numberPawnDiagRightToLeft(coordX, coordY);
				can = true;
				i = 1;
				
				if (this.validCoords(coordX+nbPawn, coordY-nbPawn) && this.board[coordX+nbPawn][coordY-nbPawn].getColor() != this.getColor()) {
					while (can && i < nbPawn) {
						if (this.validCoords(coordX+i, coordY-i) && this.board[coordX+i][coordY-i].getColor() == this.getOpponentColor())
							can = false;
						i++;
					}
					if (can) ret.add(new int[] {coordX+nbPawn, coordY-nbPawn});
				}
				
				// Remove Zen back
				if (pawnColor == PawnColor.ZEN && Player.oldZenCoords != null) {
					int found = -1;
					for (int j = 0; j < ret.size(); j++) {
						if (ret.get(j)[0] == Player.oldZenCoords[0] && ret.get(j)[1] == Player.oldZenCoords[1]) {
							found = j;
						}
					}
					if (found != -1) ret.remove(found);
				}
			}
		}

		return ret;
	}
	
	/**
	 * Display all the possibles moves on the screen
	 * @param coords The array of possible moves
	 */
	public void displayPossibleMoves(ArrayList<int[]> coords) {
		if (coords != null) {
			System.out.print("Possible moves: ");
			for (int[] moves : coords) {
				System.out.print("[" + this.coordsToInput(moves[0], moves[1]) + "] ");
			}
			System.out.println();
		} else {
			System.out.println("No possible move...");
		}
	}
	
	/**
	 * Take the input from the user and transform it in coords.
	 * @param input The input of the user
	 * @return The coords in the right way
	 */
	public int[] inputToCoords(String input) {
		int[] coords = null;
		
		if (input.matches("([1-9]|1[0-1]):[A-K]")) {
			HashMap<String, Integer> charToInt = new HashMap<String, Integer>();
			charToInt.put("A", 0);
			charToInt.put("B", 1);
			charToInt.put("C", 2);
			charToInt.put("D", 3);
			charToInt.put("E", 4);
			charToInt.put("F", 5);
			charToInt.put("G", 6);
			charToInt.put("H", 7);
			charToInt.put("I", 8);
			charToInt.put("J", 9);
			charToInt.put("K", 10);

			coords = new int[2];
			
			coords[0] = 11-Integer.valueOf(input.split(":")[0]);
			coords[1] = charToInt.get(input.split(":")[1]);
		}

		return coords;
	}
	
	/**
	 * Take coords and show it the good way to the user
	 * @param coordX Coord x
	 * @param coordY Coord y
	 * @return The coords in the right way
	 */
	public String coordsToInput(int coordX, int coordY) {
		String ret = null;
		
		if (this.validCoords(coordX, coordY)) {
			HashMap<Integer, String> intToChar = new HashMap<Integer, String>();
			intToChar.put(0, "A");
			intToChar.put(1, "B");
			intToChar.put(2, "C");
			intToChar.put(3, "D");
			intToChar.put(4, "E");
			intToChar.put(5, "F");
			intToChar.put(6, "G");
			intToChar.put(7, "H");
			intToChar.put(8, "I");
			intToChar.put(9, "J");
			intToChar.put(10, "K");
			
			StringBuilder sb = new StringBuilder();
			sb.append(Integer.toString(11-coordX));
			sb.append(":");
			sb.append(intToChar.get(coordY));
			ret = sb.toString();
		}

		return ret;
	}
	
	/**
	 * Return the color of the player
	 * @return The color of the player
	 */
	public PawnColor getColor() {
		return this.color;
	}
	
	/**
	 * Return the color of the opponent
	 * @return The color of the opponent
	 */
	public PawnColor getOpponentColor() {
		if (this.color == PawnColor.WHITE)
			return PawnColor.BLACK;
		return PawnColor.WHITE;
	}

	/**
	 * Return the color of the player
	 */
	public String toString() {
		return this.color.toString();
	}

}