package zen.model;

import java.io.InputStreamReader;
import java.util.Scanner;

public class Utils {

	public String readRules() {
		String ret = null;
		
		Scanner in = new Scanner(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(PathConfig.RULES_PATH)));
		StringBuilder sb = new StringBuilder();
		
		while (in.hasNextLine()) {
			sb.append(in.nextLine());
			sb.append(System.lineSeparator());
		}
		
		ret = sb.toString();
		in.close();
		
		return ret;
	}
	
}
