package zen.model;

public class PathConfig {
	public final static String DATA_PATH = "assets/";
	
	public final static String IMG_PATH = DATA_PATH + "img/";
	public final static String BACKGROUND_PATH = IMG_PATH + "background.jpeg";
	public final static String GAME_ICON_PATH = IMG_PATH + "icon.png";
	public final static String HOME_ICON_PATH = IMG_PATH + "home.png";
	public final static String SOUND_ICON_PATH = IMG_PATH + "sound.png";
	public final static String NOSOUND_ICON_PATH = IMG_PATH + "nosound.png";
	public final static String RESTART_ICON_PATH = IMG_PATH + "restart.png";
	
	public final static String WOOD_PATH = IMG_PATH + "wood";
	
	public final static String SONG_PATH = DATA_PATH + "song/";
	public final static String WINNER_SONG_PATH = SONG_PATH + "winner.wav";
	public final static String MOVE_SONG_PATH = SONG_PATH + "move.wav";
	public final static String ZEN_SONG_PATH = SONG_PATH + "zen.wav";
	
	public final static String RULES_PATH = DATA_PATH + "rules.txt";
}
