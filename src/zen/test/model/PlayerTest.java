package zen.test.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import zen.model.Cell;
import zen.model.PawnColor;
import zen.model.Game;
import zen.model.GameMode;
import zen.model.Human;
import zen.model.IA;

public class PlayerTest {
	
	@Test
	public void testValidCoords() {
		Human player = new Human(PawnColor.WHITE, new Cell[0][0]);
		
		assertEquals(true, player.validCoords(0, 0));
		assertEquals(true, player.validCoords(Game.SIZE-1, Game.SIZE-1));
		assertEquals(false, player.validCoords(Game.SIZE, 5));
		assertEquals(false, player.validCoords(7, -1));
	}
	
	@Test
	public void testCanSelect() {
		Game game = new Game();
		game.setGameMode(GameMode.VERSUSHUMAN);
		game.initGame();
		
		// zen
		assertEquals(true, game.getPlayer1().canSelect(5, 5));
		assertEquals(true, game.getPlayer2().canSelect(5, 5));
		
		// white
		if (game.getPlayer1().getColor() == PawnColor.WHITE) {
			assertEquals(true, game.getPlayer1().canSelect(0, 0));
			assertEquals(true, game.getPlayer1().canSelect(Game.SIZE-1, Game.SIZE-1));
		} else { // black
			assertEquals(true, game.getPlayer1().canSelect(0, Game.SIZE-1));
			assertEquals(true, game.getPlayer1().canSelect(Game.SIZE-1, 0));
		}
	}

	@Test
	public void testMovePawn() {
		Game game = new Game();
		game.setGameMode(GameMode.VERSUSHUMAN);
		game.initGame();
		Cell[][] board = game.getBoard();
		
		if (game.getCurrentPlayer().getColor() == PawnColor.BLACK) game.changeCurrent();
		
		PawnColor colPawnToMove = board[0][0].getColor();
		
		// before
		assertEquals(PawnColor.VOID, board[0][3].getColor());
		
		game.getCurrentPlayer().movePawn(0, 0, 0, 3);
		
		// after
		assertEquals(colPawnToMove, board[0][3].getColor());
		assertEquals(PawnColor.VOID, board[0][0].getColor());
	}
	
	@Test
	public void testPawnCanMove() {
		Game game = new Game();
		game.setGameMode(GameMode.VERSUSHUMAN);
		game.initGame();
		Cell[][] board = game.getBoard();
		
		// test only white
		if (game.getCurrentPlayer().getColor() == PawnColor.BLACK) game.changeCurrent();
		
		// Good
		assertEquals(true, game.getCurrentPlayer().movePawn(0, 0, 0, 3));
		
		// Void case
		assertEquals(false, game.getCurrentPlayer().movePawn(0, 1, 0, 3));
		
		// Zen (around) case
		assertEquals(false, game.getCurrentPlayer().movePawn(5, 5, 5, 7));
		
		// Move Zen at the same place
		board[5][4].setColor(PawnColor.BLACK);
		assertEquals(true, game.getCurrentPlayer().movePawn(5, 5, 5, 9));
		game.changeCurrent();
		assertEquals(false, game.getCurrentPlayer().movePawn(5, 9, 5, 5));
	}
	
	@Test
	public void testInputToCoords() {
		IA ia = new IA(PawnColor.WHITE, new Cell[0][0]);
		int[] coords = null;

		coords = ia.inputToCoords("11:D");
		assertEquals(0, coords[0]);
		assertEquals(3, coords[1]);
		
		coords = ia.inputToCoords("1:K");
		assertEquals(10, coords[0]);
		assertEquals(10, coords[1]);
		
		coords = ia.inputToCoords("0:K");
		assertEquals(null, coords);

		coords = ia.inputToCoords("5:L");
		assertEquals(null, coords);
		
		coords = ia.inputToCoords("12:G");
		assertEquals(null, coords);
	}
	
	@Test
	public void testCoordsToInput() {
		Human human = new Human(PawnColor.WHITE, new Cell[0][0]);
		String input = null;

		input = human.coordsToInput(1, 3);
		System.out.println(input);
		assertEquals(true, "10:D".equalsIgnoreCase(input));

		input = human.coordsToInput(10, 10);
		assertEquals(true, "1:K".equalsIgnoreCase(input));

		input = human.coordsToInput(11, 10);
		assertEquals(null, input);

		input = human.coordsToInput(10, 11);
		assertEquals(null, input);
		
		input = human.coordsToInput(-1, 5);
		assertEquals(null, input);
	}

	@Test
	public void testPossiblesMoves() {
		Game game = new Game();
		game.setGameMode(GameMode.VERSUSHUMAN);
		game.initGame();
		ArrayList<int[]> possibleMoves = null;
		
		if (game.getCurrentPlayer().getColor() == PawnColor.BLACK) game.changeCurrent();
		
		// test n°1
		possibleMoves = game.getCurrentPlayer().possibleMoves(0, 0);
		assertEquals(3, possibleMoves.size());

		assertEquals(3, possibleMoves.get(0)[0]);
		assertEquals(0, possibleMoves.get(0)[1]);
		assertEquals(0, possibleMoves.get(1)[0]);
		assertEquals(3, possibleMoves.get(1)[1]);
		assertEquals(3, possibleMoves.get(2)[0]);
		assertEquals(3, possibleMoves.get(2)[1]);
		
		// test n°2
		possibleMoves = game.getCurrentPlayer().possibleMoves(5, 5);
		assertEquals(null, possibleMoves);
		
		game.getBoard()[5][6] = new Cell(PawnColor.WHITE);
		
		if (game.getCurrentPlayer().getColor() == PawnColor.BLACK) game.changeCurrent();
		possibleMoves = game.getCurrentPlayer().possibleMoves(5, 5);

		assertEquals(8, possibleMoves.size());

		assertEquals(2, possibleMoves.get(0)[0]);
		assertEquals(5, possibleMoves.get(0)[1]);
		assertEquals(8, possibleMoves.get(1)[0]);
		assertEquals(5, possibleMoves.get(1)[1]);
		assertEquals(5, possibleMoves.get(2)[0]);
		assertEquals(1, possibleMoves.get(2)[1]);
		assertEquals(5, possibleMoves.get(3)[0]);
		assertEquals(9, possibleMoves.get(3)[1]);
		assertEquals(2, possibleMoves.get(4)[0]);
		assertEquals(2, possibleMoves.get(4)[1]);
		assertEquals(8, possibleMoves.get(5)[0]);
		assertEquals(8, possibleMoves.get(5)[1]);
		assertEquals(2, possibleMoves.get(6)[0]);
		assertEquals(8, possibleMoves.get(6)[1]);
		assertEquals(8, possibleMoves.get(7)[0]);
		assertEquals(2, possibleMoves.get(7)[1]);
		
		// test n°3
		game.getBoard()[5][9] = new Cell(PawnColor.BLACK);
		game.getBoard()[5][10] = new Cell(PawnColor.VOID);
		game.getBoard()[5][0] = new Cell(PawnColor.VOID);
		game.getBoard()[5][1] = new Cell(PawnColor.WHITE);
		possibleMoves = game.getCurrentPlayer().possibleMoves(5, 5);
		
		assertEquals(7, possibleMoves.size());

		assertEquals(2, possibleMoves.get(0)[0]);
		assertEquals(5, possibleMoves.get(0)[1]);
		assertEquals(8, possibleMoves.get(1)[0]);
		assertEquals(5, possibleMoves.get(1)[1]);
		assertEquals(5, possibleMoves.get(2)[0]);
		assertEquals(9, possibleMoves.get(2)[1]);
		assertEquals(2, possibleMoves.get(3)[0]);
		assertEquals(2, possibleMoves.get(3)[1]);
		assertEquals(8, possibleMoves.get(4)[0]);
		assertEquals(8, possibleMoves.get(4)[1]);
		assertEquals(2, possibleMoves.get(5)[0]);
		assertEquals(8, possibleMoves.get(5)[1]);
		assertEquals(8, possibleMoves.get(6)[0]);
		assertEquals(2, possibleMoves.get(6)[1]);
	}
	
	@Test
	public void testGetOpponentColor() {
		Human player1 = new Human(PawnColor.WHITE, new Cell[0][0]);
		assertEquals(PawnColor.BLACK, player1.getOpponentColor());
		
		Human player2 = new Human(PawnColor.BLACK, new Cell[0][0]);
		assertEquals(PawnColor.WHITE, player2.getOpponentColor());
	}

}
