package zen.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import zen.model.Cell;
import zen.model.PawnColor;
import zen.model.Difficulty;
import zen.model.Game;
import zen.model.GameMode;
import zen.model.Human;
import zen.model.IA;
import zen.model.Player;

public class GameTest {

	@Test
	public void testEnd() {
		Game game = new Game();
		game.setRunning(true);
		game.end(game.getCurrentPlayer());

		assertEquals(false, game.isRunning());
	}

	@Test
	public void testIsWinner() {
		Game game = new Game();
		game.setGameMode(GameMode.VERSUSIA);
		game.initGame();
		Cell[][] board = null;
		if (game.getCurrentPlayer().getColor() == PawnColor.BLACK) game.changeCurrent();
		
		// count(pawn) = 1
		board = new Cell[Game.SIZE][Game.SIZE];
		for (int i = 0; i < Game.SIZE; i++)
			for(int j = 0; j < Game.SIZE; j++) board[i][j] = new Cell(PawnColor.VOID);
		board[5][5].setColor(PawnColor.WHITE);
		board[10][6].setColor(PawnColor.BLACK);
		board[5][6].setColor(PawnColor.BLACK);
		game.setBoard(board);
		assertEquals(game.getPlayer1(), game.isWinner());

		// count(pawn) = 1
		for (int i = 0; i < Game.SIZE; i++)
			for(int j = 0; j < Game.SIZE; j++) board[i][j].setColor(PawnColor.VOID);
		board[5][5].setColor(PawnColor.ZEN);
		board[10][6].setColor(PawnColor.BLACK);
		board[5][6].setColor(PawnColor.BLACK);
		game.setBoard(board);
		if (game.getPlayer1().getColor() == PawnColor.WHITE)
			assertEquals(game.getPlayer1(), game.isWinner());
		else
			assertEquals(game.getPlayer2(), game.isWinner());
		
		// count(pawn) = 1
		for (int i = 0; i < Game.SIZE; i++)
			for(int j = 0; j < Game.SIZE; j++) board[i][j].setColor(PawnColor.VOID);
		board[5][5].setColor(PawnColor.ZEN);
		board[10][6].setColor(PawnColor.WHITE);
		board[5][6].setColor(PawnColor.WHITE);
		game.setBoard(board);
		if (game.getPlayer1().getColor() == PawnColor.BLACK)
			assertEquals(game.getPlayer1(), game.isWinner());
		else
			assertEquals(game.getPlayer2(), game.isWinner());
		

		// recursive (true)
		// test n°1
		for (int i = 0; i < Game.SIZE; i++)
			for(int j = 0; j < Game.SIZE; j++) board[i][j].setColor(PawnColor.VOID);
		board[0][0].setColor(PawnColor.WHITE);
		board[0][1].setColor(PawnColor.WHITE);
		board[1][2].setColor(PawnColor.ZEN);
		board[1][1].setColor(PawnColor.WHITE);
		board[2][0].setColor(PawnColor.WHITE);
		board[1][0].setColor(PawnColor.WHITE);
		board[Game.SIZE-1][Game.SIZE-1].setColor(PawnColor.BLACK);
		
		game.setBoard(board);
		assertEquals(game.getPlayer1(), game.isWinner());
		
		// test n°2
		for (int i = 0; i < Game.SIZE; i++)
			for(int j = 0; j < Game.SIZE; j++) board[i][j].setColor(PawnColor.VOID);
		board[5][2].setColor(PawnColor.WHITE);
		board[6][3].setColor(PawnColor.WHITE);
		board[7][3].setColor(PawnColor.ZEN);
		board[5][4].setColor(PawnColor.WHITE);
		board[4][5].setColor(PawnColor.WHITE);
		board[3][6].setColor(PawnColor.WHITE);
		board[Game.SIZE-1][Game.SIZE-1].setColor(PawnColor.BLACK);
		
		game.setBoard(board);
		assertEquals(game.getPlayer1(), game.isWinner());
		
		// test n°3
		for (int i = 0; i < Game.SIZE; i++)
			for(int j = 0; j < Game.SIZE; j++) board[i][j].setColor(PawnColor.VOID);
		board[Game.SIZE-2][Game.SIZE-1].setColor(PawnColor.WHITE);
		board[Game.SIZE-1][Game.SIZE-2].setColor(PawnColor.WHITE);
		board[Game.SIZE-1][Game.SIZE-3].setColor(PawnColor.WHITE);
		board[Game.SIZE-2][Game.SIZE-4].setColor(PawnColor.WHITE);
		board[Game.SIZE-3][Game.SIZE-3].setColor(PawnColor.WHITE);
		board[Game.SIZE-4][Game.SIZE-2].setColor(PawnColor.ZEN);
		board[Game.SIZE-5][Game.SIZE-1].setColor(PawnColor.WHITE);
		board[Game.SIZE-1][Game.SIZE-1].setColor(PawnColor.BLACK);
		board[Game.SIZE-2][Game.SIZE-3].setColor(PawnColor.BLACK);
		board[Game.SIZE-3][Game.SIZE-4].setColor(PawnColor.BLACK);
		
		game.setBoard(board);
		assertEquals(game.getPlayer1(), game.isWinner());
		
		// recursive (false)
		// test n°4
		for (int i = 0; i < Game.SIZE; i++)
			for(int j = 0; j < Game.SIZE; j++) board[i][j].setColor(PawnColor.VOID);
		board[0][0].setColor(PawnColor.WHITE);
		board[2][9].setColor(PawnColor.BLACK);
		board[5][0].setColor(PawnColor.WHITE);
		board[3][9].setColor(PawnColor.BLACK);
		board[10][3].setColor(PawnColor.WHITE);
		board[4][6].setColor(PawnColor.ZEN);
		board[Game.SIZE-1][Game.SIZE-1].setColor(PawnColor.BLACK);
		
		game.setBoard(board);
		assertEquals(null, game.isWinner());
		
		// test n°5
		for (int i = 0; i < Game.SIZE; i++)
			for(int j = 0; j < Game.SIZE; j++) board[i][j].setColor(PawnColor.VOID);
		board[5][2].setColor(PawnColor.WHITE);
		board[6][3].setColor(PawnColor.WHITE);
		board[8][3].setColor(PawnColor.ZEN);
		board[5][4].setColor(PawnColor.WHITE);
		board[4][5].setColor(PawnColor.WHITE);
		board[3][6].setColor(PawnColor.WHITE);
		board[Game.SIZE-1][Game.SIZE-1].setColor(PawnColor.BLACK);
		
		game.setBoard(board);
		assertEquals(null, game.isWinner());
	}
	
	@Test
	public void testSetIADifficulty() {
		Game game = new Game();
		game.setGameMode(GameMode.VERSUSIA);
		game.initGame();
		
		game.setIADifficulty(Difficulty.EASY);
		assertEquals(Difficulty.EASY, ((IA) game.getPlayer2()).getDifficulty());
		
		game.setIADifficulty(Difficulty.NORMAL);
		assertEquals(Difficulty.NORMAL, ((IA) game.getPlayer2()).getDifficulty());
		
		game.setIADifficulty(Difficulty.HARD);
		assertEquals(Difficulty.HARD, ((IA) game.getPlayer2()).getDifficulty());
	}
	
	@Test
	public void testInitGame() {
		Game game = new Game();
		game.setGameMode(GameMode.VERSUSHUMAN);
		game.initGame();
		
		assertNotNull(game.getCurrentPlayer());
		assertNotNull(game.getPlayer1());
		assertNotNull(game.getPlayer2());
		assertNotNull(game.getBoard());
		assertNotNull(game.getGameMode());
		
		assertEquals(Human.class, game.getPlayer2().getClass());
		
		game = new Game();
		game.setGameMode(GameMode.VERSUSIA);
		game.initGame();
		
		assertNotNull(game.getCurrentPlayer());
		assertNotNull(game.getPlayer1());
		assertNotNull(game.getPlayer2());
		assertNotNull(game.getBoard());
		assertNotNull(game.getGameMode());
		
		assertEquals(IA.class, game.getPlayer2().getClass());
	}
	
	@Test
	public void testChangeCurrent() {
		Game game = new Game();
		game.setGameMode(GameMode.VERSUSHUMAN);
		game.initGame();
		
		Player player1 = game.getCurrentPlayer();
		game.changeCurrent();
		Player player2 = game.getCurrentPlayer();
		
		game.changeCurrent();
		assertEquals(player1, game.getCurrentPlayer());
		game.changeCurrent();
		assertEquals(player2, game.getCurrentPlayer());
		
		assertNotEquals(player1, player2);
	}
	
	@Test
	public void testInitBoard() {
		Game game = new Game();
		game.setGameMode(GameMode.VERSUSHUMAN);
		game.initGame();
		Cell[][] board = game.getBoard();
		
		assertEquals(Game.SIZE, board.length);
		assertEquals(Game.SIZE, board[0].length);
		assertEquals(PawnColor.ZEN, board[5][5].getColor());
		assertEquals(PawnColor.WHITE, board[0][0].getColor());
		assertEquals(PawnColor.BLACK, board[0][Game.SIZE-1].getColor());
		assertEquals(PawnColor.BLACK, board[Game.SIZE-1][0].getColor());
		assertEquals(PawnColor.WHITE, board[Game.SIZE-1][Game.SIZE-1].getColor());
	}

	@Test
	public void testGetNBPawnsLeft() {
		Game game = new Game();
		game.setGameMode(GameMode.VERSUSHUMAN);
		game.initGame();
		
		assertEquals(12, game.getNBPawnsLeft(PawnColor.WHITE));
		assertEquals(12, game.getNBPawnsLeft(PawnColor.BLACK));
		
		Cell[][] newBoard = game.getBoard();
		newBoard[0][0].setColor(PawnColor.VOID); // removing one white pawn
		game.setBoard(newBoard);
		
		assertEquals(11, game.getNBPawnsLeft(PawnColor.WHITE));
		newBoard[Game.SIZE-1][Game.SIZE-1].setColor(PawnColor.VOID); // removing one white pawn
		assertEquals(10, game.getNBPawnsLeft(PawnColor.WHITE));
	}
	
	@Test
	public void testLoadSaveGame() {
		String savePath = "/tmp/game.zen";
		
		Game g1 = new Game();
		g1.setGameMode(GameMode.VERSUSHUMAN);
		g1.initGame();
		g1.setBoard(new Cell[][] {{new Cell(PawnColor.ZEN)}});
		g1.setRunning(true);
		g1.saveGame(savePath);
		
		Game g2 = new Game();
		g2.loadGame(savePath);
		
		assertEquals(g1.getBoard()[0][0].getColor(), g2.getBoard()[0][0].getColor());
		assertEquals(g1.isRunning(), g2.isRunning());
		
		assertEquals(g1.getPlayer1().getClass(), g2.getPlayer1().getClass());
		assertEquals(g1.getPlayer1().getColor(), g2.getPlayer1().getColor());
		assertEquals(g1.getPlayer2().getClass(), g2.getPlayer2().getClass());
		assertEquals(g1.getPlayer2().getColor(), g2.getPlayer2().getColor());
		assertEquals(g1.getCurrentPlayer().getClass(), g2.getCurrentPlayer().getClass());
		assertEquals(g1.getCurrentPlayer().getColor(), g2.getCurrentPlayer().getColor());
		
		assertEquals(g1.getGameMode(), g2.getGameMode());
	}

}
