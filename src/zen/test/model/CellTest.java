package zen.test.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import zen.model.Cell;
import zen.model.PawnColor;

public class CellTest {

	/**
	 * Testing the toString method
	 */
	@Test
	public void testToString() {
		Cell cell = null;
		
		cell = new Cell(PawnColor.BLACK);
		assertEquals(true, cell.toString().equals("B"));
		
		cell = new Cell(PawnColor.WHITE);
		assertEquals(true, cell.toString().equals("W"));
		
		cell = new Cell(PawnColor.ZEN);
		assertEquals(true, cell.toString().equals("Z"));
		
		cell = new Cell(PawnColor.VOID);
		assertEquals(true, cell.toString().equals("·"));
	}
}
