package zen;

import zen.model.Game;
import zen.view.FirstWindow;

public class Main {

	public static void main(String[] args) {
		Main.startGame(new Game());
	}
	
	public static void startGame(Game game) {		
		FirstWindow firstWindow = new FirstWindow();
		firstWindow.addMenu(game);
		firstWindow.addListeners(game);
		firstWindow.setVisible(true);
	}
}