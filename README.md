# Zen L'initie

Zen l'initie is a strategy board game. It can be played with 2 or 4 players. The winner is the first player who succeeds in forming a continuous chain with all his pieces still on the board and the Zen piece.

![Game image](screenshots/game.png)

## Getting Started

### Prerequisites

- Java 1.8+ [Page link](https://www.java.com/en/)

### Installing

Download the JAR file in the [releases](releases) folder.

Run this command.

```sh
$ java -jar Zen.jar
```

OR

Right click on the file and open it with Java.

## Features

- Play with 2 players
- Play against IA
- Multiple difficulties for the IA (easy, normal, hard)
- Save your game in a file
- Launch a game from a saved file
- Sound effects (On / Off)
- Display the rules

## Built With

- [Java](https://www.java.com/en/) - Programming language
- [Swing](https://docs.oracle.com/javase/7/docs/api/javax/swing/package-summary.html) - GUI widget toolkit for Java
- [JUnit](https://junit.org/) - Testing framework for Java
- [Apache ANT](https://ant.apache.org/) - Software tool for automating build processes

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details